<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class User_group_view_model extends Eloquent {

    protected $table = 'TB_V_USER_GROUP';
    public $timestamps = false;

}
