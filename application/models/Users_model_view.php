<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Users_model_view extends Eloquent {

    protected $table = 'TV_V_USER';
    public $timestamps = false;

}