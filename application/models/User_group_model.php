<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class User_group_model extends Eloquent {

    public $table = 'TV_USER_GROUP';
    public $timestamps = false;

    public function get_user_group($id){
        return  $this->where('ID_USER_GROUP', $id)->first();
    }

}
