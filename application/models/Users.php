<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Users extends Eloquent {

    protected $table = 'TV_USER';
    public $timestamps = false;

    function login($uname, $upass) {
        $pass = MD5($upass);
        $users = $this->where('USERNAME', 'like', $uname)
                    ->where('PASSWORD', 'like', $pass);
        if ($users->count() == 1) {
            return $users->first();
        } else {
            return false;
        }
    }

    function update_last_log($id) {
        date_default_timezone_set('Asia/Jakarta');
        $datetime = date("Y-m-d h:i:s");
        $user = $this->where([
                ['ID_USER', '=', $id]
        ])->update(['LAST_LOGIN' => $datetime]);
        if ($user) {
            return true;
        } else {
            return false;
        }
    }

}
