<?php

Class eoreport_model extends CI_Model {

  private $tb_name = 'TV_EO_REPORT';

  function insert_eoreport($param){

    $param['DATE_INPUT'] = date('Ymd', strtotime($param['DATE'])) ;
    $param['CREATE_AT'] = date('Ymd');
    $param['CREATED_BY'] = 'test';
    unset($param['DATE']);
    $this->db->insert($this->tb_name, $param);

    return $this->db->insert_id();
  }
  function update_eoreport($param){

    $param['DATE_INPUT'] = $param['DATE'];
    $param['CREATE_AT'] = date('Y-m-d');
    $param['CREATED_BY'] = 'test';
    unset($param['DATE']);
    $this->db->where('ID', $param['ID']);
    unset($param['ID']);
    $this->db->update($this->tb_name, $param);

    return $this->db->insert_id();
  }

  function delete_eoreport($id){
    $this->db->where('ID', $id);

    return $this->db->delete($this->tb_name);
  }


	public function get_data_list($param, $ext=null){

		$keyword = "'%%'";
		$rownum = '';
		if (isset($param['search'])) {
			// code...
			$keyword = "'%".strtolower($param['search']['value'])."%'";

			$rownum = "	 AND RowNum >= {$param['start']}
				AND RowNum < {$param['end']}";
		}

		if ($ext) {
			$where = " ";
			foreach ($ext as $key => $value) {
				$where .= "{$value['colname']} LIKE '%".strtolower($value['val'])."%'";
			}
		}else{
			$where = " (
				WEEK LIKE {$keyword} OR
				DETAIL_PROJECT LIKE {$keyword} OR
				DATE_INPUT LIKE {$keyword}
			)";
		}
		$sql = " SELECT

								*
							FROM (
								SELECT ROW_NUMBER () OVER ( ORDER BY ID_PN ) AS RowNum, * FROM
								{$this->tb_name}
							) tb
							WHERE
								$where
								$rownum

                ORDER BY WEEK ASC
						";
		$query = $this->db->query($sql);

		return $query->result_array();
	}

}
