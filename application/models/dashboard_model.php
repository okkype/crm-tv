<?php

Class Dashboard_model extends CI_Model {

    function getlistProject($customer_id = NULL) {
        // $sql = "SELECT * FROM TV_V_PROJECT_LIST";
        $customer = '';
        if ($customer_id) {
            $customer = "AND R.PARNR LIKE '$customer_id'";
        }
        $sql = "
            SELECT
                    A.MONTH,
                    A.SERNR,
                    A.PARNR,
                    C.COMPANY_NAME,
                    A.REVNR,
                    A.EQUNR,
                    A.REVBD,
                    A.REVED,
                    A.CUST_TYPE,
                    A.ATWRT,
                    A.ENGINE_APU,
                    A.REVTY,
                    A.TEAM_EO,
                    A.WORKSCOPE,
                    A.AGREED_TAT as CONTRACTUAL_TAT,
                    A.INDUCT_DATE,
                    DATEDIFF( DAY, A.ACT_START, A.ACT_FINISH ) AS ACT_TAT,
                    ( A.AGREED_TAT / ( DATEDIFF( DAY, A.ACT_START, A.ACT_FINISH )) ) AS PERFORMANCE
            FROM
                    ( SELECT
                            (
                            CASE
                                    WHEN ISDATE(D.INDUCT_DATE) = 1 THEN CAST( D.INDUCT_DATE AS DATE )
                            END ) AS MONTH, R.SERNR, R.PARNR, R.REVNR, R.EQUNR, R.REVBD, R.REVED, D.TEAM_EO, D.WORKSCOPE, D.AGREED_TAT, D.INDUCT_DATE, (
                            CASE
                                    WHEN LOWER ( R.PARNR ) LIKE '%citilink%' THEN 'GA'
                                    WHEN LOWER ( R.PARNR ) LIKE '%ga%' THEN 'GA'
                                    WHEN LOWER ( R.PARNR ) LIKE '%garuda%' THEN 'GA'
                                    ELSE ''
                            END ) AS CUST_TYPE, R.ATWRT, (
                            CASE
                                    WHEN LEFT ( R.ATWRT, 3 ) = 'CFM' THEN 'ENGINE'
                                    WHEN LEFT ( R.ATWRT, 4 ) = 'GTCP' THEN 'APU'
                                    ELSE ''
                            END ) AS ENGINE_APU, R.REVTY, (
                            CASE
                                    WHEN ISDATE( R.ATSDATE_SMR ) = 1 THEN CAST ( R.ATSDATE_SMR AS DATE )
                            END ) AS ACT_FINISH, (
                            CASE
                                    WHEN ISDATE( D.INDUCT_DATE ) = 1 THEN CAST ( D.INDUCT_DATE AS DATE )
                            END ) AS ACT_START
                    FROM
                            M_REVISION R
                    LEFT JOIN M_REVISIONDETAIL D ON
                            R.REVNR = D.REVNR
                    WHERE
                            R.IWERK = 'GAEM' AND R.TXT04 = 'REL'
                            $customer
            ) A LEFT JOIN CUSTOMER C ON
                    C.ID_CUSTOMER = A.PARNR
        ";
//        error_log($sql);
        return $this->db->query($sql)->result_array();
    }

    function getlistProjectCustomer($customer_id = NULL) {
        $sql = "
            SELECT
                    a.REVNR,
                    a.EQUNR,
                    b.TEAM_EO,
                    c.NAME1 ,
                    b.WORKSCOPE,
                    b.AGREED_TAT as CONTRACTUAL_TAT,
                    b.INDUCT_DATE,
                    SUBSTRING( a.REVBD, 1, 4 ) + '-' + SUBSTRING( a.REVBD, 5, 2 ) + '-' + SUBSTRING( a.REVBD, 7, 4 ) AS REVBD,
                    SUBSTRING( a.REVED, 1, 4 ) + '-' + SUBSTRING( a.REVED, 5, 2 ) + '-' + SUBSTRING( a.REVED, 7, 4 ) AS REVED,
                    CASE
                            WHEN a.REVED LIKE '00000000' THEN NULL
                            ELSE DATEDIFF( DAY, GETDATE(), CAST( SUBSTRING( a.REVED, 1, 4 ) + '-' + SUBSTRING( a.REVED, 5, 2 ) + '-' + SUBSTRING( a.REVED, 7, 4 ) AS DATE ) )
                    END as CURRENT_TAT
            FROM
                    M_REVISION a
            LEFT JOIN M_REVISIONDETAIL b on
                    a.REVNR = b.REVNR
            LEFT JOIN ( select
                            a.REVNR, b.VBELN, b.NAME1, b.KUNNR
                    from
                            M_PMORDER a
                    LEFT JOIN M_SALESORDER b on
                            b.VBELN = a.KDAUF
                    where
                            b.NAME1 != ''
                    group by
                            a.REVNR, b.VBELN, b.NAME1, b.KUNNR ) c on
                    a.REVNR = c.REVNR
            WHERE
                    a.TXT04 LIKE 'REL'
                ";
        if ($customer_id != NULL) {
            $sql .= "AND c.KUNNR LIKE '$customer_id'";
        }
        error_log($sql);
        return $this->db->query($sql)->result();
    }

    function getOverviewProject_($docno) {
        $query = $this->db->select('*')
                ->from('list_project')
                ->where('ESN', $docno)
                ->get();
        return $query->result();
    }

    function getOverviewProject($docno, $revnr) {
        $sql = "SELECT
                    A.MONTH,
                    A.SERNR,
                    A.PARNR,
                    A.REVNR,
                    A.EQUNR,
                    A.REVBD,
                    A.REVED,
                    A.CUST_TYPE,
                    A.ATWRT,
                    A.ENGINE_APU,
                    A.REVTY,
                    A.TEAM_EO,
                    A.WORKSCOPE,
                    A.AGREED_TAT as CONTRACTUAL_TAT,
                    A.INDUCT_DATE, 
                    DATEDIFF( DAY, A.ACT_START, A.ACT_FINISH ) AS ACT_TAT,
                    ( A.AGREED_TAT / ( DATEDIFF( DAY, A.ACT_START, A.ACT_FINISH )) ) AS PERFORMANCE 
                FROM
                    (
                    SELECT
                        ( CASE WHEN ISDATE(D.INDUCT_DATE) = 1 THEN CAST(D.INDUCT_DATE AS DATE) END ) AS MONTH,
                        R.SERNR,
                        R.PARNR,
                        R.REVNR,
                        R.EQUNR,
                        R.REVBD,
                        R.REVED,
                        D.TEAM_EO,
                        D.WORKSCOPE,
                        D.AGREED_TAT,
                        D.INDUCT_DATE, 
                        (
                    CASE
                        
                        WHEN LOWER ( R.PARNR ) LIKE '%citilink%' THEN
                        'GA' 
                        WHEN LOWER ( R.PARNR ) LIKE '%ga%' THEN
                        'GA' 
                        WHEN LOWER ( R.PARNR ) LIKE '%garuda%' THEN
                        'GA' ELSE '' 
                    END 
                        ) AS CUST_TYPE,
                        R.ATWRT,
                        (
                        CASE
                                
                                WHEN LEFT ( R.ATWRT, 3 ) = 'CFM' THEN
                                'ENGINE' 
                                WHEN LEFT ( R.ATWRT, 4 ) = 'GTCP' THEN
                                'APU' ELSE '' 
                            END 
                            ) AS ENGINE_APU,
                            R.REVTY,
                            ( CASE WHEN ISDATE( R.ATSDATE_SMR ) = 1 THEN CAST ( R.ATSDATE_SMR AS DATE ) END ) AS ACT_FINISH,
                            ( CASE WHEN ISDATE( D.INDUCT_DATE ) = 1 THEN CAST ( D.INDUCT_DATE AS DATE ) END ) AS ACT_START 
                        FROM
                            M_REVISION R
                            LEFT JOIN M_REVISIONDETAIL D ON R.REVNR = D.REVNR 
                        WHERE
                        R.IWERK = 'GAEM' AND R.REVNR = $revnr
                ) A";
        error_log($sql);
        return $this->db->query($sql)->result_array();
        // if($query->num_rows() > 0){
        //     $row    = $query->row();
        // $REVNR  = $row->REVNR;
        //     $ESN  = $row->EQUNR;
        // $EO  = $row->TEAM_EO;
        // $WORKSCOPE  = $row->WORKSCOPE;
        // $CONTRACTUAL_TAT  = $row->CONTRACTUAL_TAT;
        // $INDUCTION_DATE  = $row->INDUCT_DATE;
        // // $COMPANY_NAME  = $row->NAME1;
        // $REVBD  = $row->REVBD;
        // $REVED  = $row->REVED;
        //     $TYPE = $row->REVTY;
        //     return array($REVNR,$ESN,$EO,$WORKSCOPE,$CONTRACTUAL_TAT,$INDUCTION_DATE,$REVBD,$REVED,$TYPE);
        // }
    }

    function getListJobCard($docno, $revnr) {
        // $sql = "select ILART,ILATX,KTEXT,TXT_STAT,STATUS from TV_V_JOBCARD_PROGRESS
        // 	where EQUNR = '$docno' and REVNR = '$revnr'
        // 	and ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC')
        //              ";
        $EQUNR = ltrim($docno, '0');
        // echo $EQUNR;
        // echo $revnr;
        // die;
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                KTEXT,
                REVNR,
                EQUNR,
                ILART,
                CASE
              WHEN PHAS0 LIKE 'x' THEN
                'open'
              WHEN PHAS1 LIKE 'x' THEN
                'progress'
              WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                'close'
              END AS TXT_STAT
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
            -- AND EQUNR LIKE $EQUNR
              AND AUART LIKE 'GA01') SELECT * from BB WHERE ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC') ;";
        return $this->db->query($sql)->result();
    }

    function getTotalJC($docno, $revnr) {
        // $mat = array('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC');
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC') ;";
        // $result = $this->db->query($sql);
        // return $result->num_rows();
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    function getOpenJC($docno, $revnr) {
        // $mat = array('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC');
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr'
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC') AND TXT_STAT LIKE 'open' ;";
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    function getProgressJC($docno, $revnr) {
        // $mat = array('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC');
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC') AND TXT_STAT LIKE 'progress' ;";
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    function getClosedJC($docno, $revnr) {
        // $mat = array('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC');
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REM','PRI','DSY','1','RT1','RT2','RT3','REP','NWT','ASY','INS','TST','QEC') AND TXT_STAT LIKE 'close' ;";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    //REMOVAL
    function getTotalRemoval($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REM','PRI');";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    function getClosedRemoval($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REM','PRI') AND TXT_STAT LIKE 'close';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    //REMOVAL
    //DSY
    function getTotalDisassembly($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'DSY';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    function getClosedDisassembly($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'DSY' AND TXT_STAT LIKE 'close';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    //DSY
    //Inspection
    function getTotalInspection($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('1','RT1','RT2','RT3');";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    function getClosedInspection($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('1','RT1','RT2','RT3') AND TXT_STAT LIKE 'close';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    //Inspection
    //Repair
    function getTotalRepair($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr'
                --  AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REP','NWT');";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    function getClosedRepair($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr'
                --  AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART IN ('REP','NWT') AND TXT_STAT LIKE 'close';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    //Repair
    //ASY
    function getTotalAssembly($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'ASY';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    function getClosedAssembly($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr'
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'ASY' AND TXT_STAT LIKE 'close';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    //ASY
    //INS
    function getTotalInstall($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr'
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'INS';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    function getClosedInstall($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr'
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'INS' AND TXT_STAT LIKE 'close';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    //INS
    //TST
    function getTotalTest($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr'
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'TST';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    function getClosedTest($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr'
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'TST' AND TXT_STAT LIKE 'close';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    //TST
    //QEC
    function getTotalQEC($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'QEC';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    function getClosedQEC($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "WITH BB AS (
              SELECT DISTINCT
                AUFNR,
                AUART,
                REVNR,
                EQUNR,
                ILART,
                CASE
                    WHEN PHAS0 LIKE 'x' THEN
                    'open' 
                    WHEN PHAS1 LIKE 'x' THEN
                    'progress' 
                    WHEN PHAS3 LIKE 'x' OR PHAS2 LIKE 'x' THEN
                    'close' 
                  END AS TXT_STAT 
              FROM
                M_PMORDER
              WHERE
                REVNR LIKE '%$revnr' 
                -- AND EQUNR LIKE $EQUNR
                AND AUART LIKE 'GA01') 
                SELECT COUNT(*) as 'JUMLAH' from BB WHERE ILART = 'QEC' AND TXT_STAT LIKE 'close';";
        // echo $this->db->last_query();
        // die;
        $result = $this->db->query($sql)->row_array();
        // $count = $result['COUNT(*)'];
        return $result['JUMLAH'];
    }

    //QEC
    //DEVIATION
    function getlistDeviation($docno) {
        $sql = "
      SELECT
        RD.DEV_NUMBER,
        RD.DELAY_TAT,
        RD.DELAY_CUSTOMER,
        RD.CG_DELAY_PROCESS,
        RD.CURRENT_GATE,
        RD.NEXT_GATE_EFFCTD,
        RD.DEVIATION_REASON,
        RD.DEPARTEMENT,
        RD.PROCESS_DELAYED,
        RD.ROOT_CAUSE,
        RD.CORECTIVE_ACTION,
        RD.CREATED_BY,
        RD.CREATED_DATE
      FROM
        M_REVISIONDEVIATION RD
        JOIN M_REVISION R ON RD.REVNR = R.REVNR
      WHERE R.EQUNR = '{$docno}'
     ";
        return $this->db->query($sql)->result();
    }

    //HIGHTLIGHT
    function getlistHightlight($docno) {
        $sql = "
      SELECT
       HIGHLIGHT_NUMBER,
       APPLCBLE_MODULE,
       HIGHLIGHT,
       APPROVAL_STATUS,
       CREATED_BY,
       CREATED_DATE
      FROM
       M_REVISION
       WHERE EQUNR = '{$docno}' AND IWERK = 'GAEM' AND HIGHLIGHT_NUMBER != ''
     ";
        return $this->db->query($sql)->result();
    }

    //MANHOUR
    function getlistManhours($docno) {
        // $sql = "
        //  SELECT
        //   AUFNR,
        //   AUART,
        //   -- MAT,
        //   KTEXT,
        //   -- PHASE0-4,
        //   ARBEI,
        //   ISMNW
        //  FROM
        //   M_PMORDER
        //   WHERE EQUNR = '{$docno}' AND
        //   -- APLZL_V = 1 AND 
        //   -- RSPOS = 1
        //   GROUP BY VORNR
        // ";

        $sql = "SELECT
       AUFNR,
             AUART,
             KTEXT,
             VORNR,
       ARBEI,
       ISMNW
      FROM
       M_PMORDER
       WHERE REVNR = '{$docno}'
             AND AUART IN ('GA01','GA02')
       -- APLZL_V = 1 AND 
       -- RSPOS = 1
       GROUP BY AUFNR, AUART , KTEXT, VORNR, ARBEI, ISMNW
             ORDER BY AUFNR";

        return $this->db->query($sql)->result();
    }

    //EO REPORT
    function getlistEoReport() {
        $sql = "
      SELECT
        *
      FROM
       TV_EO_REPORT
       ORDER BY WEEK ASC
     ";
        return $this->db->query($sql)->result();
    }

    function getGSTRPMaster() {
        $sql = "select DISTINCT CONVERT( DATE, SUBSTRING(GSTRP, 0, 3) +'-'+ SUBSTRING(GSTRP, 3, 2) +'-'+ SUBSTRING(GSTRP, 5, 4), 104) as GSTRP
        from M_PMORDER
                WHERE GSTRP != '00000000'
				group by GSTRP
                ";
        return $this->db->query($sql)->result();
    }

    function getGSTRP($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "select 
                DISTINCT CONVERT( DATE, SUBSTRING(GSTRP, 0, 3) +'-'+ SUBSTRING(GSTRP, 3, 2) +'-'+ SUBSTRING(GSTRP, 5, 4), 104) as GSTRP from M_PMORDER
				where REVNR = '$revnr' and EQUNR = '$EQUNR' AND GSTRP != '00000000'
				group by GSTRP
        ORDER BY GSTRP ASC
                ";

        return $this->db->query($sql)->result();
        //       $json = json_encode ($this->db->query($sql)->result());
        // $array = ['-','{','}','[',']','"GSTRP":'];
        // //$string = str_replace($array,"", $json);
        // //return str_replace('"',"'", $string);
        // return str_replace($array,"", $json);
    }

    function getISDD($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "SELECT COUNT(AUART) as AUART,
                    CASE WHEN
                    CONVERT(DATE, SUBSTRING(GETRI, 0, 3) +'-'+ SUBSTRING(GETRI, 3, 2) +'-'+ SUBSTRING(GETRI, 5, 4), 104) is not NULL
                    THEN CONVERT(DATE, SUBSTRING(GETRI, 0, 3) +'-'+ SUBSTRING(GETRI, 3, 2) +'-'+ SUBSTRING(GETRI, 5, 4), 104)
                    ELSE 'NULL'
                    END as TGL
                from (SELECT DISTINCT
                        AUFNR,
                        AUART,
                        REVNR,
                        EQUNR,
                        ILART,
                        GETRI
                        FROM M_PMORDER
                    where REVNR = '$revnr' and EQUNR = '$EQUNR' AND GETRI != '00000000' ) tv
                group by GETRI
                order by TGL
                ";

        return $this->db->query($sql)->result_array();
        //       $json = json_encode ($this->db->query($sql)->result());
        // $array = ['-','{','}','[',']','"AUART":','"NULL"','NULL:',':,',',:'];
        // //$string = str_replace($array,"", $json);
        // //return str_replace('"',"'", $string);
        // return str_replace($array,"", $json);
    }

    function getAUART($docno, $revnr) {
        $EQUNR = ltrim($docno, '0');
        $sql = "SELECT COUNT(AUART) as AUART,
        				CASE WHEN
        				CONVERT(DATE, SUBSTRING(GSTRP, 0, 3) +'-'+ SUBSTRING(GSTRP, 3, 2) +'-'+ SUBSTRING(GSTRP, 5, 4), 104) is not NULL
        				THEN CONVERT(DATE, SUBSTRING(GSTRP, 0, 3) +'-'+ SUBSTRING(GSTRP, 3, 2) +'-'+ SUBSTRING(GSTRP, 5, 4), 104)
        				ELSE 'NULL'
        				END as TGL
				from (SELECT DISTINCT
                        AUFNR,
                        AUART,
                        REVNR,
                        EQUNR,
                        ILART,
                        GSTRP
                    FROM M_PMORDER
                    where REVNR = '$revnr' and EQUNR = '$EQUNR' AND GSTRP != '00000000') tv
				group by GSTRP
                ORDER BY TGL
                ";

        return $this->db->query($sql)->result();
        //       $json = json_encode ($this->db->query($sql)->result());
        // $array = ['-','{','}','[',']','"AUART":','"NULL"','NULL:',':,',',:'];
        // return str_replace($array,"", $json);
    }

}
