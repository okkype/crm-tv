<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Proc_model extends Eloquent {
    
    function update_customer() {
        return DB::statement("
            WITH A AS ( SELECT
                    DISTINCT KUNNR, NAME1
                FROM
                    dbo.M_SALESORDER ) MERGE INTO
                    db_crmapps.dbo.CUSTOMER
                        USING A ON
                    A.KUNNR = db_crmapps.dbo.CUSTOMER.ID_CUSTOMER
                    WHEN MATCHED THEN UPDATE
                    SET
                        COMPANY_NAME = A.NAME1
                        WHEN NOT MATCHED THEN INSERT
                            ( ID_CUSTOMER, COMPANY_NAME )
                        VALUES ( A.KUNNR, A.NAME1 );
        ");
    }
    
    function update_pmorderh() {
        return DB::statement("
            WITH A AS ( SELECT
                    DISTINCT AUFNR, REVNR, KTEXT, ILART, ERNAM, ERDAT, PHAS0, PHAS1, PHAS2, PHAS3, IDAT2
                FROM
                    dbo.M_PMORDER ) MERGE INTO
                    db_crmapps.dbo.M_PMORDERH
                        USING A ON
                    A.AUFNR = db_crmapps.dbo.M_PMORDERH.AUFNR
                    WHEN MATCHED THEN UPDATE
                    SET
                        AUFNR = A.AUFNR,
                        REVNR = A.REVNR,
                        KTEXT = A.KTEXT,
                        ILART = A.ILART,
                        ERNAM = A.ERNAM,
                        ERDAT = A.ERDAT,
                        PHAS0 = A.PHAS0,
                        PHAS1 = A.PHAS1,
                        PHAS2 = A.PHAS2,
                        PHAS3 = A.PHAS3,
                        IDAT2 = A.IDAT2;
        ");
    }
    
    function get_manhours($REVNR) {
        return DB::select("
            SELECT
                    T.AUFNR,
                    T.AUART,
                    T.KTEXT,
                    SUM( CAST( T.ARBEI AS FLOAT )) AS ARBEI,
                    SUM( CAST( T.ISMNW AS FLOAT )) AS ISMNW,
                    CASE
                            WHEN M.PHAS0 = 'X' THEN 'OPEN'
                            WHEN M.PHAS1 = 'X' THEN 'PROGRESS'
                            WHEN M.PHAS2 = 'X'
                            OR M.PHAS3 = 'X' THEN 'CLOSE'
                    END AS STATUS,
                    M.ILART
            FROM
                    (
                    SELECT
                            DISTINCT AUFNR,
                            AUART,
                            KTEXT,
                            VORNR,
                            ARBEI,
                            ISMNW
                    FROM
                            M_PMORDER
                    WHERE
                            REVNR = '$REVNR'
                            AND AUART IN ( 'GA01',
                            'GA02' ) ) T
            LEFT JOIN M_PMORDER M ON
                    T.AUFNR = M.AUFNR
            GROUP BY
                    T.AUFNR,
                    T.AUART,
                    T.KTEXT,
                    M.PHAS0,
                    M.PHAS1,
                    M.PHAS2,
                    M.PHAS3,
                    M.ILART
        ");
    }

}
