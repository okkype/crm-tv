<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname'] = '192.168.240.107';
$db['default']['username'] = 'dev-crm';
$db['default']['password'] = 'p@ssw0rd';
$db['default']['database'] = 'db_crmapps';
$db['default']['dbdriver'] = 'sqlsrv';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

$capsule = new Capsule;
$capsule->addConnection([
    'driver' => $db[$active_group]['dbdriver'] == 'mysqli' ? 'mysql' : $db[$active_group]['dbdriver'],
    'host' => $db[$active_group]['hostname'],
    'database' => $db[$active_group]['database'],
    'username' => $db[$active_group]['username'],
    'password' => $db[$active_group]['password'],
    'charset' => $db[$active_group]['char_set'],
    'collation' => $db[$active_group]['dbcollat'],
    'prefix' => $db[$active_group]['dbprefix'],
]);

// Set the event dispatcher used by Eloquent models... (optional)
//$events = new Illuminate\Events\Dispatcher;
//$events->listen('illuminate.query', function($query, $bindings, $time, $name) {
//
//    // Format binding data for sql insertion
//
//    foreach ($bindings as $i => $binding) {
//        if ($binding instanceof \DateTime) {
//            $bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
//        } else if (is_string($binding)) {
//            $bindings[$i] = "'$binding'";
//        }
//    }
//
//    // Insert bindings into query
//    $query = str_replace(array('%', '?'), array('%%', '%s'), $query);
//    $query = vsprintf($query, $bindings);
//
//    // Add it into CodeIgniter
//    $db = & get_instance()->db;
//    $db->query_times[] = $time;
//    $db->queries[] = $query;
//});
//$capsule->setEventDispatcher($events);
$capsule->setEventDispatcher(new Dispatcher(new Container));
// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();
// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

/* End of file database.php */
/* Location: ./application/config/database.php */