<?php 
	$GLOBALS['group_level'] = array(1,2,3);

	function has_session($session) 
	{
		if ( ! $session ) {
            		redirect('login/logout', 'refresh');
        	}
	}


	function is_customer($group)
	{
		return $group == 'Customer' ? true : false;
	}

	function has_access($current_path, $access_menu)
	{
		$menu_path = array();
		foreach ($access_menu as $value) 
		{
			array_push($menu_path, $value->PATH);	
		}

		if(!in_array($current_path, $menu_path))
		{
			redirect('/', 'refresh');
		} 		
	}

?>