<?php

class Dashboard extends CI_Controller {

    function __construct() {

        parent::__construct();

        $this->load->model('dashboard_model', '', TRUE);
        $this->data["session"] = $this->session->userdata('logged_in');
    }

    function index() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'bod/dashboard/index';
//        $data['listProject'] = $this->dashboard_model->getlistProjectCustomer();
        $listProject = $this->dashboard_model->getlistProject();
        $tmp = [];
        $n_result = [];
        foreach ($listProject as $key => $value) {
            $tmp = $value;
            if (isset($tmp['REVBD']) && isset($tmp['REVED'])) {
                $date1 = date('Ymd', strtotime($tmp['REVBD']));
                $date2 = date('Ymd', strtotime($tmp['REVED']));
                $start = date_create($date1);
                $end = date_create($date2);
                $tmp['CURRENT_TAT'] = (date_diff($start, $end)->format("%a"));
            } else if (isset($tmp['REVBD'])) {
                $date1 = date('Ymd', strtotime($tmp['REVBD']));
                $date2 = date('Ymd');
                $start = date_create($date1);
                $end = date_create($date2);
                $tmp['CURRENT_TAT'] = (date_diff($start, $end)->format("%a"));
            } else {
                $tmp['CURRENT_TAT'] = '-';
            }
            $n_result[] = $tmp;
        }
        $data['listProject'] = $n_result;
        $this->load->view('template', $data);
    }

    function overview($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "OVERVIEW";
        $data['content'] = 'bod/dashboard/overview';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $data['listJobCard'] = $this->dashboard_model->getListJobCard($docno, $revnr);

        $dtotalRemoval = $this->dashboard_model->getTotalRemoval($docno, $revnr);
        $closedRemoval = $this->dashboard_model->getClosedRemoval($docno, $revnr);
        $data['percentage_removal'] = @round(($closedRemoval / $dtotalRemoval) * 100);

        $totalDsy = $this->dashboard_model->getTotalDisassembly($docno, $revnr);
        $closedDsy = $this->dashboard_model->getClosedDisassembly($docno, $revnr);
        $data['percentage_disassembly'] = @round(($closedDsy / $totalDsy) * 100);

        $totalInspection = $this->dashboard_model->getTotalInspection($docno, $revnr);
        $closedInspection = $this->dashboard_model->getClosedInspection($docno, $revnr);
        $data['percentage_inspection'] = @round(($closedInspection / $totalInspection) * 100);

        $totalRepair = $this->dashboard_model->getTotalRepair($docno, $revnr);
        $closedRepair = $this->dashboard_model->getClosedRepair($docno, $revnr);
        $data['percentage_repair'] = @round(($closedRepair / $totalRepair) * 100);


        $totalAssembly = $this->dashboard_model->getTotalAssembly($docno, $revnr);
        $closedAssembly = $this->dashboard_model->getClosedAssembly($docno, $revnr);
        $data['percentage_assembly'] = @round(($closedAssembly / $totalAssembly) * 100);

        $totalInstall = $this->dashboard_model->getTotalInstall($docno, $revnr);
        $closedInstall = $this->dashboard_model->getClosedInstall($docno, $revnr);
        $data['percentage_install'] = @round(($closedInstall / $totalInstall) * 100);


        $totalTest = $this->dashboard_model->getTotalTest($docno, $revnr);
        $closedTest = $this->dashboard_model->getClosedTest($docno, $revnr);
        $data['percentage_test'] = @round(($closedTest / $totalTest) * 100);

        $totalQEC = $this->dashboard_model->getTotalQEC($docno, $revnr);
        $closedQEC = $this->dashboard_model->getClosedQEC($docno, $revnr);
        $data['percentage_qec'] = @round(($closedQEC / $totalQEC) * 100);


        $totalJobCard = $this->dashboard_model->getTotalJC($docno, $revnr);
        $totalJcOpen = $this->dashboard_model->getOpenJC($docno, $revnr);
        $totalJcProgress = $this->dashboard_model->getProgressJC($docno, $revnr);
        $totalJcClosed = $this->dashboard_model->getClosedJC($docno, $revnr);


        $data['percentage_jc_open'] = @round(($totalJcOpen / $totalJobCard) * 100);
        $data['percentage_jc_progress'] = @round(($totalJcProgress / $totalJobCard) * 100);
        $data['percentage_jc_closed'] = @round(($totalJcClosed / $totalJobCard) * 100);

        $data['totalJcClosed'] = $this->dashboard_model->getClosedJC($docno, $revnr);
        $data['totalJobCard'] = $this->dashboard_model->getTotalJC($docno, $revnr);

        $this->load->view('template', $data);
    }

    function getGSTRP() {
        $sql = "select CONVERT(varchar, CONVERT(date, GSTRP), 104) as GSTRP
from V_JOBCARD_PROGRESS
				where REVNR = '00027794' and EQUNR = '721366'
				group by GSTRP
                ";
        $json = $this->db->query($sql)->result();


        foreach ($json as $key => $value) {
            echo $key . "<br>" . "{$key} => {$value} ";
            ;
        }

        foreach ($json as $row) {
            echo $row->GSTRP . "<br>";
        }
    }

    function scurve($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "S-CURVE";
        //$data["gstrp"] = "'20180302', '3-Mar-18', '4-Mar-18', '5-Mar-18', '6-Mar-18', '7-Mar-18', '8-Mar-18'";
        $data["gstrp"] = $this->dashboard_model->getGSTRP($docno, $revnr);
        $data["plan"] = $this->dashboard_model->getAUART($docno, $revnr);
        $data["actual"] = $this->dashboard_model->getISDD($docno, $revnr);
        $data["lineIndex"] = "33.2";
        $data['content'] = 'bod/dashboard/scurve';
        $data['listGSTRP'] = $this->dashboard_model->getGSTRPMaster();
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('template', $data);
    }

    function manhours($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "MANHOURS";
        $data['content'] = 'bod/dashboard/manhours';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('template', $data);
    }

    function profit_analisyst($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "PROFIT ANALYSIST";
        $data['content'] = 'bod/dashboard/profit_analisyst';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('template', $data);
    }

    function deviation($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "DEVIATION";
        $data['content'] = 'bod/dashboard/deviation';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('template', $data);
    }

    function highlight($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "HIGHLIGHT";
        $data['content'] = 'bod/dashboard/highlight';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('template', $data);
    }

    function eo_report($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "EO-REPORT";
        $data['content'] = 'bod/dashboard/eo_report';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('template', $data);
    }

    function header_menu($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('header_menu', $data);
    }

    function save_eo_report() {
        $esn = $_POST['esn'];
        $content = $_POST['content'];


        $back = base_url() . "index.php/customer/dashboard/eo_report/$esn";

        $post_data = array(
            'ESN' => $user_id,
            'CONTENT' => $name
        );
        $this->db->insert('eo_report', $post_data);

        echo "
				<script type='text/javascript'>
				alert('Sucess');
				window.location='$back';
				</script>
			";
    }

}

/* End of file welcome.php */

/* Location: ./system/application/controllers/welcome.php */


/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
