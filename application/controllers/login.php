<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Login_users', '', TRUE);
        $this->load->model('Users');
    }

    function index() {
        $this->load->helper(array('form'));
        $this->load->view('login');
    }

    function verifylogin() {
        //This method will have the credentials validation
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

        if ($this->form_validation->run() == FALSE) {
            //Field validation failed.  User redirected to login page
            $this->load->view('login');
        }
//        else {
//            //Go to private area
//            redirect('index.php/dashboard/', 'refresh');
//        }
    }

    function check_database($password) {
        //Field validation succeeded.  Validate against database
        $username = $this->input->post('username');

        //query the database
        $result = $this->Users->login($username, $password);
        if ($result) {
            $sess_array = array();
//            foreach ($result as $row) {
            $sess_array = array(
                'id_user' => $result->ID_USER,
                'name' => $result->NAME,
                'username' => $result->USERNAME,
                'group_id' => $result->GROUP_ID,
                'customer_id' => $result->CUSTOMER_ID
            );
            $this->session->set_userdata('logged_in', $sess_array);
            $this->Users->update_last_log($sess_array['ID_USER']);
            if ($sess_array['group_id'] == '1') {
                redirect('index.php/administrator/dashboard/');
            } elseif ($sess_array['group_id'] == '2') {
                redirect('index.php/bod/dashboard/');
            } elseif ($sess_array['group_id'] == '3') {
                redirect('index.php/customer/dashboard/');
            }
//            }
//            redirect('index.php/administrator/dashboard/');
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }

    function logout() {
        $this->session->unset_userdata('logged_in');
        redirect('/', 'refresh');
    }

}

?>