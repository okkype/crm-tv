<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class eo_report extends CI_Controller {

    // Construct

    function __construct() {
        parent::__construct();
        $this->data["session"] = $this->session->userdata('logged_in');
        $this->load->model('eoreport_model', '', TRUE);
    }

    public function index() {
        echo "test routes";
    }

    function create($docno = null, $revnr = null) {
        $param = $this->input->post();
        if ($docno && $revnr) {
            $detail = "";
            foreach ($param['DETAIL_PROJECT'] as $key => $value) {
                $detail .= $value . "<br>";
            }
            $param['DETAIL_PROJECT'] = $detail;
            // if(strpos($param['DETAIL_PROJECT'], "\n") !== FALSE) {
            //   echo 'New line break found';
            // }
            // else {
            //   echo 'not found';
            // }
            //
        // die;
            $id = $this->eoreport_model->insert_eoreport($param);
            if ($id) {
                // die;
                redirect('index.php/administrator/dashboard/eo_report/' . $docno . '/' . $revnr);
            } else {
                redirect('index.php/administrator/dashboard/eo_report/' . $docno . '/' . $revnr);
            }
        } else {
            redirect('index.php/administrator/dashboard/eo_report/' . $docno . '/' . $revnr);
        }
    }

    function update($docno = null, $revnr = null, $idEo) {
        $param = $this->input->post();
        $param['ID'] = $idEo;
        if ($docno && $revnr) {
            $detail = "";
            foreach ($param['DETAIL_PROJECT'] as $key => $value) {
                $detail .= $value . "<br>";
            }
            $param['DETAIL_PROJECT'] = $detail;

            // print_r($param);
            // die;
            $id = $this->eoreport_model->update_eoreport($param);
            if ($id) {
                // die;
                redirect('index.php/administrator/dashboard/eo_report/' . $docno . '/' . $revnr);
            } else {
                redirect('index.php/administrator/dashboard/eo_report/' . $docno . '/' . $revnr);
            }
        } else {
            redirect('index.php/administrator/dashboard/eo_report/' . $docno . '/' . $revnr);
        }
    }

    function delete($id) {
        $result = $this->eoreport_model->delete_eoreport($id);
    }

}
