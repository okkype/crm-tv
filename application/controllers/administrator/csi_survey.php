<?php

class Csi_survey extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->data["session"] = $this->session->userdata('logged_in');
    }

    function index() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/csi_survey/index';
        $this->load->view('template', $data);
    }
    

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */

