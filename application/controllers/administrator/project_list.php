<?php

class Project_list extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->data["session"] = $this->session->userdata('logged_in');
        $this->data["open_menu"] = true;
    }

    function overview() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/project_list/overview';
        $data['overview'] = true;
        $this->load->view('template', $data);
    }
    function scurve() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/project_list/scurve';
        $data['scurve'] = true;
        $this->load->view('template', $data);
    }
    function manhours() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/project_list/manhours';
        $data['manhours'] = true;
        $this->load->view('template', $data);
    }
    function profit_analysis() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/project_list/profit_analysis';
        $data['profit_analysis'] = true;
        $this->load->view('template', $data);
    }
    function deviation() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/project_list/deviation';
        $data['deviation'] = true;
        $this->load->view('template', $data);
    }
    function highlight() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/project_list/highlight';
        $data['highlight'] = true;
        $this->load->view('template', $data);
    }
    function eo_report() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/project_list/eo_report';
        $data['eo_report'] = true;
        $this->load->view('template', $data);
    }

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */

