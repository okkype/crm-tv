<?php

class Dashboard extends CI_Controller {

    function __construct() {

        parent::__construct();

        $this->load->model('dashboard_model', '', TRUE);
        $this->load->model('Proc_model');
        $this->data["session"] = $this->session->userdata('logged_in');
    }

    function index() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/dashboard/index';
        $listProject = $this->dashboard_model->getlistProject();
        $tmp = [];
        $n_result = [];
        foreach ($listProject as $key => $value) {
            $tmp = $value;
            if (isset($tmp['REVBD']) && isset($tmp['REVED'])) {
                $date1 = date('Ymd', strtotime($tmp['REVBD']));
                $date2 = date('Ymd', strtotime($tmp['REVED']));
                $start = date_create($date1);
                $end = date_create($date2);
                $tmp['CURRENT_TAT'] = (date_diff($start, $end)->format("%a"));
            } else if (isset($tmp['REVBD'])) {
                $date1 = date('Ymd', strtotime($tmp['REVBD']));
                $date2 = date('Ymd');
                $start = date_create($date1);
                $end = date_create($date2);
                $tmp['CURRENT_TAT'] = (date_diff($start, $end)->format("%a"));
            } else {
                $tmp['CURRENT_TAT'] = '-';
            }
            $n_result[] = $tmp;
        }
        $data['listProject'] = $n_result;
        // print_r($tmp);
        // die;
        $this->load->view('template', $data);
    }

    function overview($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "OVERVIEW";
        $data['content'] = 'administrator/dashboard/overview';
        $listProject = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $tmp = [];
        $n_result = [];
        foreach ($listProject as $key => $value) {
            $tmp = $value;
            if (isset($tmp['REVBD']) && isset($tmp['REVED'])) {
                $date1 = date('Ymd', strtotime($tmp['REVBD']));
                $date2 = date('Ymd', strtotime($tmp['REVED']));
                $start = date_create($date1);
                $end = date_create($date2);
                $tmp['CURRENT_TAT'] = (date_diff($start, $end)->format("%a"));
                $data['PlanSVDate'] = $tmp['CURRENT_TAT'];
            } else if (isset($tmp['REVBD'])) {
                $date1 = date('Ymd', strtotime($tmp['REVBD']));
                $date2 = date('Ymd');
                $start = date_create($date1);
                $end = date_create($date2);
                $tmp['CURRENT_TAT'] = (date_diff($start, $end)->format("%a"));
                $data['PlanSVDate'] = '-';
            } else {
                $tmp['CURRENT_TAT'] = '-';
                $data['PlanSVDate'] = '-';
            }
            $data['ACT_TAT'] = $tmp['ACT_TAT'];
            $data['ESN'] = $tmp['SERNR'];
            $data['TEAM_EO'] = $tmp['TEAM_EO'];
            $data['WORKSCOPE'] = $tmp['WORKSCOPE'];
            $data['REVTY'] = $tmp['REVTY'];
            $data['INDUCT_DATE'] = $tmp['INDUCT_DATE'];
            $data['CONTRACTUAL_TAT'] = $tmp['CONTRACTUAL_TAT'];
            $data['ATWRT'] = $tmp['ATWRT'];
            $data['ENGINE_APU'] = $tmp['ENGINE_APU'];
            $n_result[] = $tmp;
        }
        $data['listProject'] = $n_result;
        $data['listJobCard'] = $this->dashboard_model->getListJobCard($docno, $revnr);

        $dtotalRemoval = $this->dashboard_model->getTotalRemoval($docno, $revnr);
        $closedRemoval = $this->dashboard_model->getClosedRemoval($docno, $revnr);
        $data['percentage_removal'] = @round(($closedRemoval / $dtotalRemoval) * 100);

        $totalDsy = $this->dashboard_model->getTotalDisassembly($docno, $revnr);
        $closedDsy = $this->dashboard_model->getClosedDisassembly($docno, $revnr);
        $data['percentage_disassembly'] = @round(($closedDsy / $totalDsy) * 100);

        $totalInspection = $this->dashboard_model->getTotalInspection($docno, $revnr);
        $closedInspection = $this->dashboard_model->getClosedInspection($docno, $revnr);
        $data['percentage_inspection'] = @round(($closedInspection / $totalInspection) * 100);

        $totalRepair = $this->dashboard_model->getTotalRepair($docno, $revnr);
        $closedRepair = $this->dashboard_model->getClosedRepair($docno, $revnr);
        $data['percentage_repair'] = @round(($closedRepair / $totalRepair) * 100);


        $totalAssembly = $this->dashboard_model->getTotalAssembly($docno, $revnr);
        $closedAssembly = $this->dashboard_model->getClosedAssembly($docno, $revnr);
        $data['percentage_assembly'] = @round(($closedAssembly / $totalAssembly) * 100);

        $totalInstall = $this->dashboard_model->getTotalInstall($docno, $revnr);
        $closedInstall = $this->dashboard_model->getClosedInstall($docno, $revnr);
        $data['percentage_install'] = @round(($closedInstall / $totalInstall) * 100);


        $totalTest = $this->dashboard_model->getTotalTest($docno, $revnr);
        $closedTest = $this->dashboard_model->getClosedTest($docno, $revnr);
        $data['percentage_test'] = @round(($closedTest / $totalTest) * 100);

        $totalQEC = $this->dashboard_model->getTotalQEC($docno, $revnr);
        $closedQEC = $this->dashboard_model->getClosedQEC($docno, $revnr);
        $data['percentage_qec'] = @round(($closedQEC / $totalQEC) * 100);


        $totalJobCard = $this->dashboard_model->getTotalJC($docno, $revnr);
        $totalJcOpen = $this->dashboard_model->getOpenJC($docno, $revnr);
        $totalJcProgress = $this->dashboard_model->getProgressJC($docno, $revnr);
        $totalJcClosed = $this->dashboard_model->getClosedJC($docno, $revnr);
        // echo $totalJcClosed,'-';
        // echo $totalJcProgress;
        // echo $totalJobCard;


        $data['percentage_jc_open'] = @round(($totalJcOpen / $totalJobCard) * 100);
        $data['percentage_jc_progress'] = @round(($totalJcProgress / $totalJobCard) * 100);
        $data['percentage_jc_closed'] = @round(($totalJcClosed / $totalJobCard) * 100);
        // echo $data['percentage_jc_closed'];
        // die;

        $data['totalJcClosed'] = $this->dashboard_model->getClosedJC($docno, $revnr);
        $data['totalJobCard'] = $this->dashboard_model->getTotalJC($docno, $revnr);
        // echo $totalJcClosed;
        // die;

        $this->load->view('template', $data);
    }

    // function getGSTRP() {
    // 		$sql = "select CONVERT(varchar, CONVERT(date, GSTRP), 104) as GSTRP
    //                from V_JOBCARD_PROGRESS
    // 			where REVNR = '00027794' and EQUNR = '721366'
    // 			group by GSTRP
    //                ";
    //        $json = $this->db->query($sql)->result();
    // 	foreach($json as $key=>$value){
    // 			echo $key . "<br>" . "{$key} => {$value} ";;
    // 		}
    // 		foreach($json as $row){
    // 			echo $row->GSTRP . "<br>";
    // 		}
    //    }

    function scurve($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "S-CURVE";
        //$data["gstrp"] = "'20180302', '3-Mar-18', '4-Mar-18', '5-Mar-18', '6-Mar-18', '7-Mar-18', '8-Mar-18'";
        $gstrp = $this->dashboard_model->getGSTRP($docno, $revnr);

        // echo "<br>";
        // echo "<br>";
        $data_gstrp = $this->_array_clear($gstrp, 'GSTRP');
        // $data_gstrp = $gstrp;

        array_push($data_gstrp, date('d-m-Y'));
        $data['gstrp'] = json_encode($data_gstrp);

        // print_r($data_gstrp);
        // die;
        // print_r($data['gstrp']);
        // print_r( $this->_array_clear($gstrp, 'GSTRP') );

        $plan = $this->dashboard_model->getAUART($docno, $revnr);
        $tmp = [];
        if ($plan) {
            $tPlan = $this->_array_clear($plan, 'AUART');
            // $tPlan = $plan;
            // print_r($tPlan);

            $tmp = $tPlan;
            $juml = $tmp[0];
            for ($i = 1; $i < count($tPlan); $i++) {
                $juml += $tPlan[$i];
                $tmp[$i] = $juml;
            }
            array_push($tmp, $juml);
        }
        // print_r($tmp);
        // echo "<br>";
        // echo "<br>";
        $data['plan'] = json_encode($tmp);
        // print_r($data['plan']);
        $tmp = [];
        $n_result_actual = [];
        $actual = $this->dashboard_model->getISDD($docno, $revnr);
        if (isset($actual)) {
            foreach ($actual as $key => $value) {
                $tmp = $value;
                $tmp['cumulative'] = 0;
                foreach ($actual as $key2 => $value2) {
                    if ($value2['TGL'] <= $tmp['TGL']) {
                        $tmp['cumulative'] += $value2['AUART'];
                    }
                }
                $i = $i + 1;
                $n_result_actual[] = $tmp;
            }
        }
        $tmp2 = [];
        foreach ($n_result_actual as $rows) {
            $tmp2[] = $rows['cumulative'];
        }
        // echo "<br>";
        // echo "<br>";

        $data['actual'] = json_encode($tmp2);
        //  print_r($data['actual']);
        // die;
        // $data['actual'] = json_encode($this->_array_clear($n_result_actual, 'cumulative'));
        // $today = [];
        // for ($i=0; $i < count($data_gstrp); $i++) { 
        //     array_push($today, $actual[0]);
        // }
        // $data['today'] = json_encode($today);
        // print_r($data['actual']);

        $data["lineIndex"] = count($data_gstrp) - 1;
        $data['content'] = 'administrator/dashboard/scurve';
        $data['listGSTRP'] = $this->dashboard_model->getGSTRPMaster();
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $data['ESN'] = $data['listProject'][0]['SERNR'];
        $data['TEAM_EO'] = $data['listProject'][0]['TEAM_EO'];
        $data['WORKSCOPE'] = $data['listProject'][0]['WORKSCOPE'];
        $data['REVTY'] = $data['listProject'][0]['REVTY'];
        $data['INDUCT_DATE'] = $data['listProject'][0]['INDUCT_DATE'];
        $data['CONTRACTUAL_TAT'] = $data['listProject'][0]['CONTRACTUAL_TAT'];
        $data['ATWRT'] = $data['listProject'][0]['ATWRT'];
        $data['ENGINE_APU'] = $data['listProject'][0]['ENGINE_APU'];
        // print_r($data);
        // die;
        $this->load->view('template', $data);
    }

    private function _array_clear($data, $val = NULL) {
        $tmp = [];
        foreach ($data as $key => $value) {
            $tmp[] = $value->{$val};
        }
        return $tmp;
    }

    function manhours($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "MANHOURS";
        $data['content'] = 'administrator/dashboard/manhours';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $data['ESN'] = $data['listProject'][0]['SERNR'];
        $data['TEAM_EO'] = $data['listProject'][0]['TEAM_EO'];
        $data['WORKSCOPE'] = $data['listProject'][0]['WORKSCOPE'];
        $data['REVTY'] = $data['listProject'][0]['REVTY'];
        $data['INDUCT_DATE'] = $data['listProject'][0]['INDUCT_DATE'];
        $data['CONTRACTUAL_TAT'] = $data['listProject'][0]['CONTRACTUAL_TAT'];
        $data['ATWRT'] = $data['listProject'][0]['ATWRT'];
        $data['ENGINE_APU'] = $data['listProject'][0]['ENGINE_APU'];
//        $listManhour = $this->dashboard_model->getlistManhours($revnr);
        $listManhour = $this->Proc_model->get_manhours($revnr);

        $tmp = [];
        $manhour_plan = 0;
        $manhour_actual = 0;

        $manhour_jobcard = 0;
        $manhour_mdr = 0;

        $act_jobcard = 0;
        $act_mdr = 0;

        foreach ($listManhour as $key => $value) {

            $manhour_plan += (float) $value->ARBEI;
            $manhour_actual += (float) $value->ISMNW;

            $tmp = $value;
            if ($value->AUART == "GA02") {

                $tmp->MAT = 'MDR';
                $manhour_mdr += (float) $value->ARBEI;
                $act_mdr += (float) $value->ISMNW;
            } else {
                $tmp->MAT = 'JOB CARD';
                $manhour_jobcard += (float) $value->ARBEI;
                $act_jobcard += (float) $value->ISMNW;
            }

            $data['listManhour'][] = $tmp;
        }
        $data['listManhour'] = $listManhour;

        $data['chart']['total'] = "[{$manhour_plan},{$manhour_actual}]";
        $data['chart']['jobcard'] = "[{$manhour_jobcard},{$act_jobcard}]";
        $data['chart']['mdr'] = "[{$manhour_mdr},{$act_mdr}]";

        $this->load->view('template', $data);
    }

    function profit_analisyst($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "PROFIT ANALYSIST";
        $data['content'] = 'administrator/dashboard/profit_analisyst';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $data['ESN'] = $data['listProject'][0]['SERNR'];
        $data['TEAM_EO'] = $data['listProject'][0]['TEAM_EO'];
        $data['WORKSCOPE'] = $data['listProject'][0]['WORKSCOPE'];
        $data['REVTY'] = $data['listProject'][0]['REVTY'];
        $data['INDUCT_DATE'] = $data['listProject'][0]['INDUCT_DATE'];
        $data['CONTRACTUAL_TAT'] = $data['listProject'][0]['CONTRACTUAL_TAT'];
        $data['ATWRT'] = $data['listProject'][0]['ATWRT'];
        $data['ENGINE_APU'] = $data['listProject'][0]['ENGINE_APU'];
        $this->load->view('template', $data);
    }

    function deviation($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "DEVIATION";
        $data['content'] = 'administrator/dashboard/deviation';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $data['ESN'] = $data['listProject'][0]['SERNR'];
        $data['TEAM_EO'] = $data['listProject'][0]['TEAM_EO'];
        $data['WORKSCOPE'] = $data['listProject'][0]['WORKSCOPE'];
        $data['REVTY'] = $data['listProject'][0]['REVTY'];
        $data['INDUCT_DATE'] = $data['listProject'][0]['INDUCT_DATE'];
        $data['CONTRACTUAL_TAT'] = $data['listProject'][0]['CONTRACTUAL_TAT'];
        $data['ATWRT'] = $data['listProject'][0]['ATWRT'];
        $data['ENGINE_APU'] = $data['listProject'][0]['ENGINE_APU'];
        $data['listDeviation'] = $this->dashboard_model->getlistDeviation($docno);
        $count_gate = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        // $count_gate = [4,2,9,12,51,3,7,1,9];
        foreach ($data['listDeviation'] as $key => $value) {

            $count_gate[($value->CURRENT_GATE - 1)] = $count_gate[($value->CURRENT_GATE - 1)] + 1;
        }
        $str_count_gate = '[';
        foreach ($count_gate as $key => $value) {
            $str_count_gate .= $value . ',';
        }
        $str_count_gate = substr($str_count_gate, 0, -1);
        $str_count_gate .= ']';
        $data['count_gate'] = $str_count_gate;
        // die;
        // print_r($data);
        $this->load->view('template', $data);
    }

    function highlight($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "HIGHLIGHT";
        $data['content'] = 'administrator/dashboard/highlight';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $data['ESN'] = $data['listProject'][0]['SERNR'];
        $data['TEAM_EO'] = $data['listProject'][0]['TEAM_EO'];
        $data['WORKSCOPE'] = $data['listProject'][0]['WORKSCOPE'];
        $data['REVTY'] = $data['listProject'][0]['REVTY'];
        $data['INDUCT_DATE'] = $data['listProject'][0]['INDUCT_DATE'];
        $data['CONTRACTUAL_TAT'] = $data['listProject'][0]['CONTRACTUAL_TAT'];
        $data['ATWRT'] = $data['listProject'][0]['ATWRT'];
        $data['ENGINE_APU'] = $data['listProject'][0]['ENGINE_APU'];
        $data['listHightlight'] = $this->dashboard_model->getlistHightlight($docno);
        // echo  $this->db->last_query();
        // die;

        $status = [0, 0];
        // $status = [4,2,9,12,51,3,7,1,9];
        foreach ($data['listHightlight'] as $key => $value) {

            if ($value->APPROVAL_STATUS == 'CL') {
                $status[1] = $status[1] + 1;
            } else if ($value->APPROVAL_STATUS == 'CR') {
                $status[0] = $status[0] + 1;
            }
        }
        $str_status = '[';
        foreach ($status as $key => $value) {
            $str_status .= $value . ',';
        }
        $str_status = substr($str_status, 0, -1);
        $str_status .= ']';
        $data['status'] = $str_status;
        // print_r($data);
        // die;
        $this->load->view('template', $data);
    }

    function eo_report($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "EO-REPORT";
        $data['content'] = 'administrator/dashboard/eo_report';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $data['ESN'] = $data['listProject'][0]['SERNR'];
        $data['TEAM_EO'] = $data['listProject'][0]['TEAM_EO'];
        $data['WORKSCOPE'] = $data['listProject'][0]['WORKSCOPE'];
        $data['REVTY'] = $data['listProject'][0]['REVTY'];
        $data['INDUCT_DATE'] = $data['listProject'][0]['INDUCT_DATE'];
        $data['CONTRACTUAL_TAT'] = $data['listProject'][0]['CONTRACTUAL_TAT'];
        $data['ATWRT'] = $data['listProject'][0]['ATWRT'];
        $data['ENGINE_APU'] = $data['listProject'][0]['ENGINE_APU'];
        $data['listEoReport'] = $this->dashboard_model->getlistEoReport();
        $this->load->view('template', $data);
    }

    function header_menu($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);

        $this->load->view('header_menu', $data);
    }

    function save_eo_report() {
        $esn = $_POST['esn'];
        $content = $_POST['content'];


        $back = base_url() . "index.php/administrator/dashboard/eo_report/$esn";

        $post_data = array(
            'ESN' => $user_id,
            'CONTENT' => $name
        );
        $this->db->insert('eo_report', $post_data);

        echo "
				<script type='text/javascript'>
				alert('Sucess');
				window.location='$back';
				</script>
			";
    }

}

/* End of file welcome.php */

/* Location: ./system/application/controllers/welcome.php */


/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
