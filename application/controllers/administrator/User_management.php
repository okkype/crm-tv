<?php

require_once('assets/editablegrid/EditableGrid.php');

class User_management extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('auth_helper');
        $this->load->model('Users');
        $this->load->model('Customer_model');
//        $this->load->model('Work_area_model');
        $this->load->model('User_group_model');
        $this->load->model('Users_model_view');
//        $this->load->model('User_group_view_model');
//        $this->load->model('Work_area_view_model');
//        $this->load->model('Menu_model');
//        $this->load->model('Menu_access_model');
//        $this->load->model('Menu_access_view_model');


        $this->data["session"] = $this->session->userdata('logged_in');
        has_session($this->data["session"]);

        $GROUP_ID = $this->data["session"]["group_id"];
//        $user_menu['side_bar'] = $this->Menu_access_view_model->getMenuByGroup($GROUP_ID, 'S');
//        $user_menu['tab_menu'] = $this->Menu_access_view_model->getMenuByGroup($GROUP_ID, 'T');
//        $this->session->set_userdata('menu', $user_menu);
    }

    function index() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'user_management/index';
        $data['group'] = $this->User_group_model->where('ID_USER_GROUP', $data["session"]['group_id'])->first();
        $data['group'] = $this->User_group_model->where('ID_USER_GROUP', $data["session"]['group_id'])->first();
        $data['listUser'] = $this->user_management_model->getListUser();
        $data['user_management'] = true;
        $this->load->view('template', $data);
    }

    /* start of User Management
     * ********************************************************************************************************************** */

    public function crud_users() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/user_management/crud_users';
//        $data['group'] = $this->User_group_model->where('ID_USER_GROUP', $data["session"]['group_id'])->first();
        $data['user_management'] = true;

        $data['customer'] = $this->Customer_model->get();
//        $data['work_area'] = $this->Work_area_model->get();
        $data['user_group'] = $this->User_group_model->get();
        $data['current_menu'] = 'Users Management';
        $this->load->view('template', $data);
    }

    public function crud_project_member() {
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('project_member');
        $crud->set_subject('');
        $crud->columns('Name', 'Position', 'Email', 'Contact_Person', 'id_project');
        $crud->fields('Name', 'Position', 'Email', 'Contact_Person', 'id_project');
        $crud->display_as('id_project', 'Project Name');
        $crud->set_relation('id_project', 'project', 'project_name');
        $crud->required_fields('Name', 'Position', 'Email', 'Contact_Person');

        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/user_management/crud_project_member';
        $data['group'] = $this->User_group_model->where('ID_USER_GROUP', $data["session"]['group_id'])->first();
        $data['user_management'] = true;

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function encrypt_password($post_array) {
        $this->load->helper('security');
        $post_array['PASSWORD'] = do_hash($post_array['PASSWORD'], 'md5');
        $post_array['STATUS'] = 1;
        return $post_array;
    }

    public function crud_user_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('NAME', 'Name', 'string', NULL, false);
        $grid->addColumn('USERNAME', 'Username', 'string', NULL, false);
        $grid->addColumn('NAME1', 'Company Name', 'String', NULL, false);
        $grid->addColumn('USER_GROUP', 'User Group', 'string', NULL, false);
//        $grid->addColumn('WORK_AREA', 'Work Area', 'string', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'ID_USER');

        $active_job = $this->Users_model_view;

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];

        $rowByPage = 20;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('NAME1', 'like', '%' . $filter . '%')
                    ->orwhere('NAME', 'like', '%' . $filter . '%')
                    ->orwhere('USERNAME', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('NAME', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function add_new_users() {
        $this->load->helper('security');
        $msg["status"] = NULL;
        $msg["body_msg"] = NULL;

        $user = $this->Users;
        try {
            $user->NAME = $_POST['name'];
            $user->USERNAME = $_POST['username'];
            $user->PASSWORD = md5($_POST['password']);
            $user->CUSTOMER_ID = $_POST['customer_id'];
            $user->GROUP_ID = $_POST['group_id'];
            $user->STATUS = 1;
//            $user->WORK_AREA_ID = $_POST['work_area_id'];
            $user->save();

            $msg["status"] = "success";
            $msg["body_msg"] = "User has been saved";
        } catch (Exception $e) {
            $msg["status"] = "failed";
            $msg["body_msg"] = $e;
        }
        echo json_encode($msg);
    }

    public function get_user_by_id() {
        $msg["status"] = "success";
        $msg["data"] = NULL;
        $msg["body_msg"] = NULL;
        if (isset($_GET['id'])) {
            try {
                $id = $_GET['id'];
                $msg["data"] = $this->Users->where("ID_USER", $id)->first();
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    public function update_user() {
        $msg["status"] = NUll;
        $msg["body_msg"] = NUll;
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $user = $this->Users->where('ID_USER', $id);
            try {
                $user->update(array(
                    'NAME' => $_POST['name'],
                    'USERNAME' => $_POST['username'],
                    'PASSWORD' => md5($_POST['password']),
                    'GROUP_ID' => $_POST['group_id'],
                    'CUSTOMER_ID' => $_POST['customer_id']
                ));
                $msg["status"] = "success";
                $msg["body_msg"] = "User has been saved";
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    public function delete_user_by_id() {
        $msg["status"] = NULL;
        $msg["body_msg"] = NULL;
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            try {
                $user = $this->Users->where('ID_USER', $id);
                $user = $user->delete();

                $msg["status"] = "success";
                $msg["body_msg"] = "user has been deleted successfully";
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    /* start of User Group
     * ********************************************************************************************************************** */

    public function user_group() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/user_management/user_group';
        $data['group'] = $this->User_group_model->where('ID_USER_GROUP', $data["session"]['group_id'])->first();
        $data['user_management'] = true;
        $data['list_menu'] = $this->Menu_model->get();

        $data['current_menu'] = 'User Group';
        $this->load->view('template', $data);
    }

    public function user_group_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('USER_GROUP', 'Name', 'string', NULL, false);
        $grid->addColumn('LEVEL', 'Level', 'string', NULL, false);
        $grid->addColumn('USER_COUNT', 'Total User', 'string', NULL, false);
        $grid->addColumn('menu', 'Menu', 'html', NULL, false, 'ID_USER_GROUP');
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'ID_USER_GROUP');

        $active_job = $this->User_group_view_model;

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        // if (isset($_GET['page']) && is_numeric($_GET['page']))
        //     $page = (int) $_GET['page'];

        $rowByPage = 20;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('USER_GROUP', 'like', '%' . $filter . '%')
                    ->orwhere('user_count', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('USER_GROUP', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function add_new_users_group() {
        $msg["status"] = NULL;
        $msg["body_msg"] = NULL;

        $user = $this->User_group_model;
        try {
            $user->USER_GROUP = $_POST['name'];
            $user->LEVEL = $_POST['level'];
            $user->save();

            $msg["status"] = "success";
            $msg["body_msg"] = "User Group has been saved";
        } catch (Exception $e) {
            $msg["status"] = "failed";
            $msg["body_msg"] = $e;
        }
        echo json_encode($msg);
    }

    public function get_user_group_by_id() {
        $msg["status"] = "success";
        $msg["data"] = NULL;
        $msg["body_msg"] = NULL;
        if (isset($_GET['id'])) {
            try {
                $id = $_GET['id'];
                $msg["data"] = $this->User_group_model->where("ID_USER_GROUP", $id)->first();
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    public function update_user_group() {
        $msg["status"] = NUll;
        $msg["body_msg"] = NUll;
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $user = $this->User_group_model->where('ID_USER_GROUP', $id);
            try {
                $user->update(array(
                    'USER_GROUP' => $_POST['name'],
                ));
                $msg["status"] = "success";
                $msg["body_msg"] = "User has been saved";
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    public function delete_user_group_by_id() {
        $msg["status"] = NULL;
        $msg["body_msg"] = NULL;
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            try {
                $user = $this->User_group_model->where('ID_USER_GROUP', $id);
                $user = $user->delete();

                $msg["status"] = "success";
                $msg["body_msg"] = "user group has been deleted successfully";
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    /* start of Work Area
     * ********************************************************************************************************************** */

    public function work_area() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/user_management/work_area';
        $data['group'] = $this->User_group_model->where('ID_USER_GROUP', $data["session"]['group_id'])->first();
        $data['user_management'] = true;

        $data['current_menu'] = 'Work Area';
        $this->load->view('template', $data);
    }

    public function work_area_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('WORK_AREA', 'Name', 'string', NULL, false);
        $grid->addColumn('USER_COUNT', 'Total User', 'string', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'ID_WORK_AREA');

        $active_job = $this->Work_area_view_model;

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        // if (isset($_GET['page']) && is_numeric($_GET['page']))
        //     $page = (int) $_GET['page'];

        $rowByPage = 20;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('WORK_AREA', 'like', '%' . $filter . '%')
                    ->orwhere('user_count', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('WORK_AREA', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function add_new_work_area() {
        $msg["status"] = NULl;
        $msg["body_msg"] = NULl;

        $user = $this->Work_area_model;
        try {
            $user->ID_WORK_AREA = $_POST['id'];
            $user->WORK_AREA = $_POST['name'];
            $user->save();

            $msg["status"] = "success";
            $msg["body_msg"] = "Work area has been saved";
        } catch (Exception $e) {
            $msg["status"] = "failed";
            $msg["body_msg"] = $e;
        }
        echo json_encode($msg);
    }

    public function get_work_area_by_id() {
        $msg["status"] = "success";
        $msg["data"] = NULL;
        $msg["body_msg"] = NULL;
        if (isset($_GET['id'])) {
            try {
                $id = $_GET['id'];
                $msg["data"] = $this->Work_area_model->where("ID_WORK_AREA", $id)->first();
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    public function update_work_area() {
        $msg["status"] = NUll;
        $msg["body_msg"] = NUll;
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $user = $this->Work_area_model->where('ID_WORK_AREA', $id);
            try {
                $user->update(array(
                    'WORK_AREA' => $_POST['name'],
                ));
                $msg["status"] = "success";
                $msg["body_msg"] = "Work Area has been saved";
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    public function delete_work_area_by_id() {
        $msg["status"] = NULL;
        $msg["body_msg"] = NULL;
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            try {
                $user = $this->Work_area_model->where('ID_WORK_AREA', $id);
                $user = $user->delete();

                $msg["status"] = "success";
                $msg["body_msg"] = "work area has been deleted successfully";
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    /* start of Menu management
     * ********************************************************************************************************************** */

    public function menu_management() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/user_management/menu_management';
        $data['group'] = $this->User_group_model->where('ID_USER_GROUP', $data["session"]['group_id'])->first();
        $data['user_management'] = true;
        $data['list_menu'] = $this->Menu_model->get();

        $data['current_menu'] = 'Menu Management';
        $this->load->view('template', $data);
    }

    public function menu_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('LABEL', 'Label', 'string', NULL, false);
        $grid->addColumn('PATH', 'Path', 'string', NULL, false);
        $grid->addColumn('TYPE', 'Type', 'string', NULL, false);
        $grid->addColumn('PARENT', 'Parent', 'string', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'MENU_ID');

        $active_job = $this->Menu_model;

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        // if (isset($_GET['page']) && is_numeric($_GET['page']))
        //     $page = (int) $_GET['page'];

        $rowByPage = 20;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('LABEL', 'like', '%' . $filter . '%')
                    ->orwhere('user_count', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('LABEL', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function add_new_menu() {
        $msg["status"] = NULL;
        $msg["body_msg"] = NULL;

        $user = $this->Menu_model;
        try {
            $user->LABEL = $_POST['LABEL'];
            $user->PATH = $_POST['PATH'];
            $user->PARENT = $_POST['PARENT'];
            $user->TYPE = $_POST['TYPE'];
            $user->ICON = $_POST['ICON'];
            $user->save();

            $msg["status"] = "success";
            $msg["body_msg"] = "menu has been saved";
        } catch (Exception $e) {
            $msg["status"] = "failed";
            $msg["body_msg"] = $e;
        }
        echo json_encode($msg);
    }

    public function get_menu_by_id() {
        $msg["status"] = "success";
        $msg["data"] = NULL;
        $msg["body_msg"] = NULL;
        if (isset($_GET['id'])) {
            try {
                $id = $_GET['id'];
                $msg["data"] = $this->Menu_model->where("MENU_ID", $id)->first();
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    public function update_menu() {
        $msg["status"] = NUll;
        $msg["body_msg"] = NUll;
        if (isset($_POST['MENU_ID'])) {
            $id = $_POST['MENU_ID'];
            $user = $this->Menu_model->where('MENU_ID', $id);
            try {
                $user->update(array(
                    'LABEL' => $_POST['LABEL'],
                    'PATH' => $_POST['PATH'],
                    'PARENT' => $_POST['PARENT'],
                    'TYPE' => $_POST['TYPE'],
                    'ICON' => $_POST['ICON']
                ));
                $msg["status"] = "success";
                $msg["body_msg"] = "Menu has been Updated";
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    public function delete_menu_by_id() {
        $msg["status"] = NULL;
        $msg["body_msg"] = NULL;
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            try {
                $user = $this->Menu_model->where('MENU_ID', $id);
                $user = $user->delete();

                $msg["status"] = "success";
                $msg["body_msg"] = "menu has been deleted successfully";
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    /* start of Menu Access
     * ********************************************************************************************************************** */

    public function menu_access() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'administrator/user_management/menu_management';
        $data['group'] = $this->User_group_model->where('ID_USER_GROUP', $data["session"]['group_id'])->first();
        $data['user_management'] = true;
        $data['list_menu'] = $this->Menu_model->get();

        $data['current_menu'] = 'Menu Management';
        $this->load->view('template', $data);
    }

    public function menu_access_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('LABEL', 'Label', 'string', NULL, false);
        $grid->addColumn('READ_ONLY', 'Path', 'string', NULL, false);
        $grid->addColumn('TYPE', 'Type', 'string', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'MENU_ACCESS_ID');

        $active_job = $this->Menu_access_view_model;

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        // if (isset($_GET['page']) && is_numeric($_GET['page']))
        //     $page = (int) $_GET['page'];

        $rowByPage = 20;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('LABEL', 'like', '%' . $filter . '%')
                    ->orwhere('user_count', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "") {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('LABEL', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);
        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function add_new_menu_access() {
        $msg["status"] = NULL;
        $msg["body_msg"] = NULL;

        $user = $this->Menu_access_model;
        try {
            $user->MENU_ID = $_POST['MENU_ID'];
            $user->READ_ONLY = $_POST['READ_ONLY'];
            $user->USER_GROUP_ID = $_POST['USER_GROUP_ID'];
            $user->save();

            $msg["status"] = "success";
            $msg["body_msg"] = "menu has been saved";
            $msg["USER_GROUP_ID"] = $_POST['USER_GROUP_ID'];
        } catch (Exception $e) {
            $msg["status"] = "failed";
            $msg["body_msg"] = $e;
        }
        echo json_encode($msg);
    }

    public function get_menu_access_by_id() {
        $msg["status"] = NULL;
        $msg["data"] = NULL;
        $msg["body_msg"] = NULL;
        if (isset($_GET['id'])) {
            try {
                $id = $_GET['id'];
                $msg["data"] = $this->Menu_access_view_model->where('USER_GROUP_ID', $id)->get();
                $msg["status"] = "success";
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    public function update_menu_access() {
        $msg["status"] = NUll;
        $msg["body_msg"] = NUll;
        if (isset($_POST['MENU_ID'])) {
            $id = $_POST['MENU_ID'];
            $user = $this->Menu_model->where('MENU_ID', $id);
            try {
                $user->update(array(
                    'LABEL' => $_POST['LABEL'],
                    'PATH' => $_POST['PATH'],
                    'PARENT' => $_POST['PARENT'],
                    'TYPE' => $_POST['TYPE']
                ));
                $msg["status"] = "success";
                $msg["body_msg"] = "Menu has been saved";
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    public function delete_menu_access_by_id() {
        $msg["status"] = NULL;
        $msg["body_msg"] = NULL;
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            try {
                $user = $this->Menu_access_model->where('MENU_ACCESS_ID', $id);
                $user = $user->delete();

                $msg["status"] = "success";
                $msg["body_msg"] = "menu has been deleted successfully";
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

    public function getAllMenu() {
        $msg["status"] = "success";
        $msg["data"] = NULL;
        $msg["body_msg"] = NULL;
        if (isset($_GET['id'])) {
            try {
                $id = $_GET['id'];
                $msg["data"] = $this->Menu_model->where("MENU_ID", $id)->first();
            } catch (Exception $e) {
                $msg["status"] = "failed";
                $msg["body_msg"] = $e;
            }
        }
        echo json_encode($msg);
    }

}
