<?php

class Dashboard extends CI_Controller {

    function __construct() {

        parent::__construct();

        $this->load->model('dashboard_model', '', TRUE);
        $this->data["session"] = $this->session->userdata('logged_in');
    }

    function index() {
        $data["session"] = $this->session->userdata('logged_in');
        $customer_id = $data["session"]["customer_id"];
//        error_log(json_encode($data["session"]));
        //var_dump($customer_id);exit();
        $data['content'] = 'customer/dashboard/index';
//        $data['listProject'] = $this->dashboard_model->getlistProjectCustomer($customer_id);
        $listProject = $this->dashboard_model->getlistProject($customer_id);
        $tmp = [];
        $n_result = [];
        foreach ($listProject as $key => $value) {
            $tmp = $value;
            if (isset($tmp['REVBD']) && isset($tmp['REVED'])) {
                $date1 = date('Ymd', strtotime($tmp['REVBD']));
                $date2 = date('Ymd', strtotime($tmp['REVED']));
                $start = date_create($date1);
                $end = date_create($date2);
                $tmp['CURRENT_TAT'] = (date_diff($start, $end)->format("%a"));
            } else if (isset($tmp['REVBD'])) {
                $date1 = date('Ymd', strtotime($tmp['REVBD']));
                $date2 = date('Ymd');
                $start = date_create($date1);
                $end = date_create($date2);
                $tmp['CURRENT_TAT'] = (date_diff($start, $end)->format("%a"));
            } else {
                $tmp['CURRENT_TAT'] = '-';
            }
            $n_result[] = $tmp;
        }
        $data['listProject'] = $n_result;
        $this->load->view('template', $data);
    }

    function overview($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "OVERVIEW";
        $data['content'] = 'customer/dashboard/overview';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $data['listJobCard'] = $this->dashboard_model->getListJobCard($docno, $revnr);

        $dtotalRemoval = $this->dashboard_model->getTotalRemoval($docno, $revnr);
        $closedRemoval = $this->dashboard_model->getClosedRemoval($docno, $revnr);
        $data['percentage_removal'] = @round(($closedRemoval / $dtotalRemoval) * 100);

        $totalDsy = $this->dashboard_model->getTotalDisassembly($docno, $revnr);
        $closedDsy = $this->dashboard_model->getClosedDisassembly($docno, $revnr);
        $data['percentage_disassembly'] = @round(($closedDsy / $totalDsy) * 100);

        $totalInspection = $this->dashboard_model->getTotalInspection($docno, $revnr);
        $closedInspection = $this->dashboard_model->getClosedInspection($docno, $revnr);
        $data['percentage_inspection'] = @round(($closedInspection / $totalInspection) * 100);

        $totalRepair = $this->dashboard_model->getTotalRepair($docno, $revnr);
        $closedRepair = $this->dashboard_model->getClosedRepair($docno, $revnr);
        $data['percentage_repair'] = @round(($closedRepair / $totalRepair) * 100);


        $totalAssembly = $this->dashboard_model->getTotalAssembly($docno, $revnr);
        $closedAssembly = $this->dashboard_model->getClosedAssembly($docno, $revnr);
        $data['percentage_assembly'] = @round(($closedAssembly / $totalAssembly) * 100);

        $totalInstall = $this->dashboard_model->getTotalInstall($docno, $revnr);
        $closedInstall = $this->dashboard_model->getClosedInstall($docno, $revnr);
        $data['percentage_install'] = @round(($closedInstall / $totalInstall) * 100);


        $totalTest = $this->dashboard_model->getTotalTest($docno, $revnr);
        $closedTest = $this->dashboard_model->getClosedTest($docno, $revnr);
        $data['percentage_test'] = @round(($closedTest / $totalTest) * 100);

        $totalQEC = $this->dashboard_model->getTotalQEC($docno, $revnr);
        $closedQEC = $this->dashboard_model->getClosedQEC($docno, $revnr);
        $data['percentage_qec'] = @round(($closedQEC / $totalQEC) * 100);


        $totalJobCard = $this->dashboard_model->getTotalJC($docno, $revnr);
        $totalJcOpen = $this->dashboard_model->getOpenJC($docno, $revnr);
        $totalJcProgress = $this->dashboard_model->getProgressJC($docno, $revnr);
        $totalJcClosed = $this->dashboard_model->getClosedJC($docno, $revnr);


        $data['percentage_jc_open'] = @round(($totalJcOpen / $totalJobCard) * 100);
        $data['percentage_jc_progress'] = @round(($totalJcProgress / $totalJobCard) * 100);
        $data['percentage_jc_closed'] = @round(($totalJcClosed / $totalJobCard) * 100);

        $data['totalJcClosed'] = $this->dashboard_model->getClosedJC($docno, $revnr);
        $data['totalJobCard'] = $this->dashboard_model->getTotalJC($docno, $revnr);

        $this->load->view('template', $data);
    }

    function getGSTRP() {
        $sql = "select CONVERT(varchar, CONVERT(date, GSTRP), 104) as GSTRP
from V_JOBCARD_PROGRESS
				where REVNR = '00027794' and EQUNR = '721366'
				group by GSTRP
                ";
        $json = $this->db->query($sql)->result();


        foreach ($json as $key => $value) {
            echo $key . "<br>" . "{$key} => {$value} ";
            ;
        }

        foreach ($json as $row) {
            echo $row->GSTRP . "<br>";
        }
    }

    function scurve($docno, $revnr) {

        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "S-CURVE";
        //$data["gstrp"] = "'20180302', '3-Mar-18', '4-Mar-18', '5-Mar-18', '6-Mar-18', '7-Mar-18', '8-Mar-18'";
        $gstrp = $this->dashboard_model->getGSTRP($docno, $revnr);


        // echo "<br>";
        // echo "<br>";
        $data_gstrp = $this->_array_clear($gstrp, 'GSTRP');
        array_push($data_gstrp, date('d-m-Y'));
        $data['gstrp'] = json_encode($data_gstrp);
        // print_r($data['gstrp']);
        // print_r( $this->_array_clear($gstrp, 'GSTRP') );

        $plan = $this->dashboard_model->getAUART($docno, $revnr);
        $tmp = [];
        if ($plan) {
            $tPlan = $this->_array_clear($plan, 'AUART');
            // print_r($tPlan);

            $tmp = $tPlan;
            $juml = $tmp[0];
            for ($i = 1; $i < count($tPlan); $i++) {
                $juml += $tPlan[$i];
                $tmp[$i] = $juml;
            }
            array_push($tmp, $juml);
        }
        // print_r($tmp);
        // echo "<br>";
        // echo "<br>";
        $data['plan'] = json_encode($tmp);
        // print_r($data['plan']);
        $tmp = [];
        $n_result_actual = [];
        $actual = $this->dashboard_model->getISDD($docno, $revnr);
        if (isset($actual)) {
            foreach ($actual as $key => $value) {
                $tmp = $value;
                $tmp['cumulative'] = 0;
                foreach ($actual as $key2 => $value2) {
                    if ($value2['TGL'] <= $tmp['TGL']) {
                        $tmp['cumulative'] += $value2['AUART'];
                    }
                }
                $i = $i + 1;
                $n_result_actual[] = $tmp;
            }
        }
        $tmp2 = [];
        foreach ($n_result_actual as $rows) {
            $tmp2[] = $rows['cumulative'];
        }
        // echo "<br>";
        // echo "<br>";

        $data['actual'] = json_encode($tmp2);
        //  print_r($data['actual']);
        // die;
        // $data['actual'] = json_encode($this->_array_clear($n_result_actual, 'cumulative'));
        // $today = [];
        // for ($i=0; $i < count($data_gstrp); $i++) { 
        //     array_push($today, $actual[0]);
        // }
        // $data['today'] = json_encode($today);
        // print_r($data['actual']);

        $data["lineIndex"] = count($data_gstrp) - 1;
        $data['content'] = 'customer/dashboard/scurve';
        $data['listGSTRP'] = $this->dashboard_model->getGSTRPMaster();
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        // print_r($data);
        // die;
        $this->load->view('template', $data);
    }

    function manhours($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "MANHOURS";
        $data['content'] = 'customer/dashboard/manhours';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('template', $data);
    }

    function profit_analisyst($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "PROFIT ANALYSIST";
        $data['content'] = 'customer/dashboard/profit_analisyst';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('template', $data);
    }

    function deviation($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "DEVIATION";
        $data['content'] = 'customer/dashboard/deviation';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $data['deviation'] = $this->dashboard_model->get_deviation($revnr);
        $this->load->view('template', $data);
    }

    function highlight($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "HIGHLIGHT";
        $data['content'] = 'customer/dashboard/highlight';
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('template', $data);
    }

    function eo_report($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data["session"] = $this->session->userdata('logged_in');
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "EO-REPORT";
        $data['content'] = 'customer/dashboard/eo_report';

        $data['report'] = $this->dashboard_model->get_report($docno);
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('template', $data);
    }

    function header_menu($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data['listProject'] = $this->dashboard_model->getOverviewProject($docno, $revnr);
        $this->load->view('header_menu', $data);
    }

    function csi_survey($docno, $revnr) {
        $data["docno"] = $docno;
        $data["revnr"] = $revnr;
        $data['header_menu'] = 'header_menu';
        $data['title_menu'] = 'title_menu';
        $data['title'] = "CSI Survey";
        $data["esn"] = $docno;
        $data["survey"] = $this->dashboard_model->view_survey($docno);
        $data['content'] = 'customer/dashboard/csi_survey';
        $this->load->view('template', $data);
    }

    function submit_survey() {
        $user_id = $this->data["session"]['id_user'];
        $name = $_POST['name'];
        $company = $_POST['company'];
        $email = $_POST['email'];
        $esn = $_POST['product_esn'];
        $quality_rating = $_POST['quality-rating'];
        $quality_reason = $_POST['quality-reason'];
        $tat_rating = $_POST['tat-rating'];
        $tat_reason = $_POST['tat-reason'];
        $price_rating = $_POST['price-rating'];
        $price_reason = $_POST['price-reason'];
        $services_rating = $_POST['services-rating'];
        $timelines_rating = $_POST['timelines-rating'];
        $services_reason = $_POST['services-reason'];
        $overall_rating = $_POST['overall-rating'];
        $overall_reason = $_POST['overall-reason'];
        $documentation_rating = $_POST['documentation-rating'];
        $documentation_reason = $_POST['documentation-reason'];
        $project_reason = $_POST['project-reason'];

        $back = base_url() . "index.php/customer/csi_survey";

        $post_data = array(
            'USER_ID' => $user_id,
            'NAME' => $name,
            'COMPANY' => $company,
            'EMAIL' => $email,
            'ESN' => $esn,
            'QUALITY_RATE' => $quality_rating,
            'QUALITY_REASON' => $quality_reason,
            'TAT_RATE' => $tat_rating,
            'TAT_REASON' => $tat_reason,
            'PRICE_RATE' => $price_rating,
            'PRICE_REASON' => $price_reason,
            'SERVICES_RATE' => $services_rating,
            'TIMELINES_RATE' => $timelines_rating,
            'SERVICES_REASON' => $services_reason,
            'OVERALL_RATE' => $overall_rating,
            'OVERALL_REASON' => $overall_reason,
            'DOCUMENTATION_RATE' => $documentation_rating,
            'DOCUMENTATION_REASON' => $documentation_reason,
            'PROJECT_REASON' => $project_reason,
        );
        $this->db->insert('csi_survey', $post_data);

        echo "
				<script type='text/javascript'>
				alert('Sucess');
				window.location='$back';
				</script>
			";
    }

    function save_eo_report() {
        $esn = $_POST['esn'];
        $content = $_POST['content'];


        $back = base_url() . "index.php/customer/dashboard/eo_report/$esn";

        $post_data = array(
            'ESN' => $user_id,
            'CONTENT' => $name
        );
        $this->db->insert('eo_report', $post_data);

        echo "
				<script type='text/javascript'>
				alert('Sucess');
				window.location='$back';
				</script>
			";
    }

}

/* End of file welcome.php */

/* Location: ./system/application/controllers/welcome.php */


/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
