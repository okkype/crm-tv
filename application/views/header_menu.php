<div class="row">
    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
        <a style="padding: 8px 40px;" class="btn btn-rounded waves-effect btn-social btn-block btn-default" href= "<?php echo base_url(); ?>index.php/administrator/dashboard/" href='javascript:void(0);'>
            <i class="fa fa-home"></i> Dashboard
        </a>
    </div>
    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12" style="width: 100%;">
        <div>
            <a style="padding: 8px 40px; width: 14%;" class="btn btn-outline-primary btn-rounded waves-effect btn-social bg-green" href= "<?php echo base_url(); ?>index.php/administrator/dashboard/overview/<?php echo $docno; ?>/<?php echo $revnr; ?>" href='javascript:void(0);'>
                <i class="fa fa-file"></i> Overview
            </a>
            <a style="padding: 8px 40px; width: 14%;" class="btn btn-outline-primary btn-rounded waves-effect btn-social bg-orange" href= "<?php echo base_url(); ?>index.php/administrator/dashboard/scurve/<?php echo $docno; ?>/<?php echo $revnr; ?>" href='javascript:void(0);'> <i class="fa fa-area-chart"></i>S-Curve </a>
            <?php
            if ($this->data["session"]["group_id"] == "1") {
                ?>
                <a style="padding: 8px 40px; width: 14%;" class="btn btn-outline-primary btn-rounded waves-effect btn-social bg-green" href= "<?php echo base_url(); ?>index.php/administrator/dashboard/manhours/<?php echo $docno; ?>/<?php echo $revnr; ?>" href='javascript:void(0);'> <i class="fa fa-bell"></i>Man Hours</a>
                <?php
            }
            ?>
            <a style="padding: 8px 40px; width: 14%;" class="btn btn-outline-primary btn-rounded waves-effect btn-social bg-green" href= "<?php echo base_url(); ?>index.php/administrator/dashboard/deviation/<?php echo $docno; ?>/<?php echo $revnr; ?>" href='javascript:void(0);'> <i class="fa fa-file"></i>Deviation </a>
            <a style="padding: 8px 40px; width: 14%;" class="btn btn-outline-primary btn-rounded waves-effect btn-social bg-orange " href= "<?php echo base_url(); ?>index.php/administrator/dashboard/highlight/<?php echo $docno; ?>/<?php echo $revnr; ?>" href='javascript:void(0);'>
                <i class="glyphicon glyphicon-pushpin"></i> Highlight
            </a>
            <a style="padding: 8px 40px; width: 14%;" class="btn btn-outline-primary btn-rounded waves-effect btn-social bg-green" href= "<?php echo base_url(); ?>index.php/administrator/dashboard/eo_report/<?php echo $docno; ?>/<?php echo $revnr; ?>" href='javascript:void(0);'>
                <i class="fa fa-book"></i> EO Report
            </a>
            <a style="padding: 8px 40px; width: 14%;" class="btn btn-outline-primary btn-rounded waves-effect btn-social bg-orange"  href= "<?php echo base_url(); ?>index.php/administrator/csi_survey" href='javascript:void(0);'>
                <i class="fa fa-table"></i> Survey
            </a>
        </div>
    </div>
</div>