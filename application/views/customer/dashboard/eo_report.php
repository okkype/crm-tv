<style>
.n-padding {
  padding: 0px;
  padding-bottom: 5px;
}
</style>
<section class="content">
    <?php
    $this->load->view($header_menu);
    $this->load->view($title_menu);
    ?>

    <div class="container-fluid">
      <div class="row">
        <div>
          <div class="box">
            <div class="box-body">
              <div class="col-md-12">
                <form id="form-report" action="<?= site_url('index.php/administrator/eo_report/create/'.$docno.'/'.$revnr) ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                  <div class="form-group">
                    <label for="week" class="col-sm-3 control-label"> week</label>
                    <div class="col-md-4">
                      <input type="number" class="form-control" id="WEEK" name="WEEK">

                    </div>
                  </div>
                  <div class="form-group">
                    <label for="detail_report" class="col-sm-3 control-label"> Detail Report</label>
                    <div class="col-md-9 no-padding">
                      <div class="col-md-12" id="group-detail">

                      </div>
                      <div class="col-md-8 ">
                        <input class="form-control" id="DETAIL_PROJECT" type="text">
                      </div>
                      <div class="col-md-4">
                        <button class="btn btn-primary btn-sm" id="add-new-detail" type="button"><i class="fa fa-plus"></i></button>
                        <!-- <button class="btn btn-danger btn-sm" id="remove-new-detail" type="button"><i class="fa fa-times"></i></button> -->
                      </div>
                      <!-- <textarea class="form-control" id="DETAIL_PROJECT" name="DETAIL_PROJECT" style="min-height: 130px;"></textarea> -->
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="date" class="col-sm-3 control-label"> Date</label>
                    <div class="col-md-4">
                      <input class="form-control" type="date" id="DATE" name="DATE">
                    </div>
                  </div>
                  <input name="EQUNR" hidden="true" value="<?= $docno ?>">
                  <div class="col-md-12">
                    <div id="button-submit" class="pull-right">
                      <button onclick="btSubmit()" class="btn btn-success pull=right" id="btnSubmit" type="button" ><i class="fa fa-save"> </i> Save </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div >
          <div class="box">
            <div class="box-body">
              <div class=" table-responsive ">
                <table id="tbReport" class="table table-striped table-bordered table-hovered " >
                  <thead>
                    <tr>
                      <th> Week</th>
                      <th> Detail Report</th>
                      <th> Date</th>
                      <th> ID</th>
                      <th> Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $no = 0;
                    if (is_array($listEoReport)) {
                     foreach ($listEoReport as $row) {
                    $no++;
                     ?>

                     <tr>
                       <td> Week <?= $row->WEEK ?></td>
                       <td>
                         <?php
                          str_replace("\n","<br>",$row->DETAIL_PROJECT);
                          echo $row->DETAIL_PROJECT; ?>
                       </td>
                       <td> <?= $row->DATE_INPUT ?> </td>
                       <td> <?= $row->ID ?> </td>
                       <td>
                         <button onclick="btUpdate(this, '<?= $row->ID ?>')"class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></button>
                         <button onclick="removeReport('<?= $row->ID ?>')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                       </td>
                     </tr>

                    <?php }} ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<script type="text/javascript">
  var table;
  var count = 1;
  $(function(){

    // table = $('#tbreport').Datatable();
    table = $('#tbReport').DataTable({
        "columnDefs": [
            {
                "targets": [ 3 ],
                "visible": false,
                "searchable": false
            }
        ]
    } );
    $('#add-new-detail').on('click', function(){
        var $val = $('#DETAIL_PROJECT');
        if ($val.val()) {

          addrow($val.val(), "#group-detail");
          $( $val ).parent( "div" ).removeClass('has-error');
          $val.val('');
        }else{
          $( $val ).parent( "div" ).addClass('has-error');
          $val.focus();
        }
    });


  });
  function remove(id){
    var elem = document.getElementById(id);
    return elem.parentNode.removeChild(elem);
  }
  var week;
  function btSubmit(){

    week = $('#form-report #WEEK').val();
    if (week) {

      var data = table.rows().data();

      result = data.filter(function (x) {  temp = "Week "+week;  return (x[0] == temp);} );
      console.log(result);

      if (result.length!=0) {
        alertify.alert('Warning !! ', 'Week '+week+' is Already Created. Do you want to replace ?', function(){
            id = result[0][3];
            $('#form-report').prop('action', "<?= site_url('index.php/administrator/eo_report/update/'.$docno.'/'.$revnr) ?>/"+id);
            $('#form-report').submit();
        });
      }else{
        $('#form-report').submit();
      }

    }
  }
  function btCancel(){

        // $('#button-submit #form-cancel').on('click', function(){
            $('#form-report').prop('action', "<?= site_url('index.php/administrator/eo_report/create/'.$docno.'/'.$revnr) ?>/");
            $('#form-report #WEEK').val('');
            $('#form-report #group-detail').empty();
            $('#form-report #DATE').val('');

            $('#form-report #btnSubmit').removeClass("btn-warning");
            $('#form-report #btnSubmit').addClass("btn-primary");
            $('#form-report #btnSubmit').html("");
            $('#form-report #btnSubmit').html("<i class='fa fa-save'></i> Add ");
            $('#form-report #button-submit #form-cancel').remove();

        // });

  }
  function btUpdate(a, id){
    $('#form-report #WEEK').val('');
    $('#form-report #DATE').val('');
    $('#form-report #group-detail').empty();

    $('#form-report #button-submit #form-cancel').remove();
    var data = $(a).closest('tr');

    week = data[0].cells[0].innerText;
    group = data[0].cells[1].innerText;
    date = data[0].cells[2].innerText;
    // id = data[0].cells[3].innerText;
    week = (week.split(' '))[1];
    group = group.split('\n');
    // console.log(group);

    // if ($('#form-cancel').length == 0) {
      $('#form-report').prop('action', "<?= site_url('index.php/administrator/eo_report/update/'.$docno.'/'.$revnr) ?>/"+id);
      $('#form-report #WEEK').val(parseInt(week));

      $.each(group, function(index, val) {
          if (val) {
            addrow(val, "#group-detail");
          }
      });
      $('#form-report #DATE').val(date);
      $('#form-report #btnSubmit').removeClass("btn-primary");
      $('#form-report #btnSubmit').addClass("btn-warning");
      $('#form-report #btnSubmit').attr("onclick", "");
      $('#form-report #btnSubmit').attr("type", "submit");
      $('#form-report #btnSubmit').html("");
      $('#form-report #btnSubmit').html("<i class='fa fa-save'></i> Update ");
      $('#form-report #button-submit').append('<button onclick="btCancel()" id="form-cancel" class="btn" type="button"> Cancel </button>');

      $( '#form-report #WEEK').focus();
    // }

  }
  function removeReport(id) {
    $.ajax({
      type: 'POST',
      url : "<?=  site_url('index.php/administrator/eo_report/delete') ?>/"+id,
      success: function(res){
        location.reload();
      }

    });
  }
  function addrow($val, $tag){

    $($tag).append(
      '<div id="col'+count+'">'+
        '<div class="col-md-8 n-padding">'+

            // '<div class="col-md-9">'+
              '<input class="form-control" name="DETAIL_PROJECT['+count+']" type="text" value="'+$val+'">'+
            // '</div>'+

        '</div>'+
        '<div class="col-md-4">'+
          '<button class="btn btn-danger btn-sm btRemove" onclick="remove(\'col'+count+'\')" type="button"><i class="fa fa-times"></i></button>'+
        '</div>'+
      '</div>'
    );
    count++;
  }
</script>
