
<style type="text/css">
.content {
        font-family: "segoeui";
        src: url("assets/bower_components/font-awesome/fonts/segoeui.ttf");
    }
</style>

<section class="content">
<?php 
$this->load->view($header_menu); 
$this->load->view($title_menu); 
?>
<div class="container-fluid">
 <div class="row">
        <div class="col-md-12">
          <!-- Line chart -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

          <!--     <h3 class="box-title">Line Chart</h3> -->

              <!-- <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div> -->
            <div style="width:100%; margin:0 auto;">
              <!-- <canvas id="line-chart" width="800" height="450"></canvas> -->
              <canvas id="myChart" width="1366" height="600"></canvas><br>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->


        </div>

      </div>
</div>
</div>

</section>
<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>
<script>

// new Chart(document.getElementById("line-chart"), {
//   type: 'line',
//   data: {
//     labels: ["2-Oct","4-Oct","6-Oct","8-Oct","10-Oct","12-Oct","14-Oct","16-Oct","18-Oct"],
//     datasets: [{ 
//         data: [168,170,178,190,203,276,408,547,675,734],
//         label: "Plan",
//         borderColor: "#3cba9f",
//         fill: false
//       }, { 
//         data: [130,145,150,170,200,220,320,485,547,640],
//         label: "Actual",
//         borderColor: "#c45850",
//         fill: false
//       }
//     ]
//   },
//   options: {
//     title: {
//       display: true,
//       // text: 'World population per region (in millions)'
//     }
//   }
// });


var originalLineDraw = Chart.controllers.line.prototype.draw;
Chart.helpers.extend(Chart.controllers.line.prototype, {
  draw: function() {
    originalLineDraw.apply(this, arguments);

    var chart = this.chart;
    var ctx = chart.chart.ctx;

    var index = chart.config.data.lineAtIndex;
    if (index) {
      var xaxis = chart.scales['x-axis-0'];
      var yaxis = chart.scales['y-axis-0'];

      ctx.save();
      ctx.beginPath();
      ctx.moveTo(xaxis.getPixelForValue(undefined, index), yaxis.top);
      ctx.strokeStyle = '#38e144';
      ctx.lineTo(xaxis.getPixelForValue(undefined, index), yaxis.bottom);
      ctx.stroke();
      ctx.restore();
    }
  }
});

var config = {
  type: 'line',
  data: {
    labels: <?php echo $gstrp; ?>,
    datasets: [{
      label: "PLAN",
      data: <?php echo $plan; ?>,
      borderColor: "#42a5f6",
      fill: false
    }, 

    {
      label: "ACTUAL", 
      data: <?php echo $actual; ?>,
      borderColor: "#ff5622",
      fill: false
    },
	 {
      label: "TODAY", 
      data: <?= $actual ?>,
      borderColor: "#38e144",
      fill: false
    }],
    lineAtIndex: <?php echo $lineIndex; ?> 
  }
};

var ctx = document.getElementById("myChart").getContext("2d");
new Chart(ctx, config);
</script>