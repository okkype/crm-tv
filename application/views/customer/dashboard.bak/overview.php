<?php list($REVNR,$ESN,$EO,$WORKSCOPE,$CONTRACTUAL_TAT,$INDUCTION_DATE,$CURRENT_TAT,$COMPANY_NAME,$REVBD,$REVED) = $listProject; ?>
<style type="text/css">
.content {
        font-family: "Helvetica";
        src: url("assets/bower_components/font-awesome/fonts/HelveticaLTStd-Roman_0.otf");
    }
</style>

<section class="content">
<?php 
$this->load->view($header_menu); 
$this->load->view($title_menu); 
?>

<div class="container-fluid">
<div class="row">
    <div class="col-sm-4">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
                  <div class="info-box">
                      <span class="info-box-icon" style="background-color: #65b32b;"><i class="fa fa-file-text-o"></i></span>
                      <div class="info-box-content">
                      <span class="info-box-text">Actual TAT</span>
                      <span class="info-box-number" style="font-size: 30px">26 Days</span>
                   </div>
                </div>
              <!-- /.info-box -->
                <div class="info-box">
                <span class="info-box-icon" style="background-color: #f3e812;"><i class="fa fa-file-text"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Foreseen TAT</span>
                    <span class="info-box-number" style="font-size: 30px">30 Days</span>
                    <span class="info-box-text">Plan Serviceable Date<br>
                    <b style="font-size: 20px;"><?php 
									$source = $REVED;
									$date = new DateTime($source);
									echo $date->format('d-m-Y'); // 31-07-2012
								?></b></span>
                </div>
                </div>

              <div class="info-box">
                <span class="info-box-icon" style="background-color: #0dacc1;"><i class="fa fa-list-alt"></i></span>
                   <div class="info-box-content">
                      <span class="info-box-text">Job Card Completion </span>
                      <span class="info-box-number" style="font-size: 30px"><?php echo $percentage_jc_closed; ?>%</span>
                      <span class="info-box-text"><?php echo $totalJcClosed; ?> of <?php echo $totalJobCard; ?></span>
                   </div>
                </div>

                <div class="col-sm-12">
                  <p>Removal:
                      <div class="progress">
                          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage_removal; ?>%; background-color:#08aebe;">
                          <?php echo $percentage_removal; ?> %
                          </div>
                      </div>
                  </p>
                  <p>Disassembly:
                      <div class="progress">
                          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage_disassembly; ?>%; background-color:#08aebe;">
                          <?php echo $percentage_disassembly; ?>%
                         </div>
                      </div>
                  </p>
                  <p>Inspection:
                      <div class="progress">
                          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage_inspection; ?>%; background-color:#08aebe;">
                          <?php echo $percentage_inspection; ?>%
                          </div>
                      </div>
                  </p>
                  <p>Repair:
                       <div class="progress">
                          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage_repair; ?>%; background-color:#08aebe;">
                          <?php echo $percentage_repair; ?>%
                          </div>
                      </div>
                  </p>
                  <p>Assembly:
                         <div class="progress">
                          <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage_assembly; ?>%; background-color:#08aebe;">
                          <?php echo $percentage_assembly; ?>%
                          </div>
                      </div>
                  </p>
                  <p>Install:
                        <div class="progress">
                          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage_install; ?>%; background-color:#08aebe;">
                          <?php echo $percentage_install; ?>%
                          </div>
                      </div>
                  </p>
                  <p>Test:
                         <div class="progress">
                          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage_test; ?>%; background-color:#08aebe;">
                          <?php echo $percentage_test; ?>%
                          </div>
                      </div>
                  </p>
                    <p>QEC:
                         <div class="progress">
                          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage_qec; ?>%; background-color:#08aebe;">
                          <?php echo $percentage_qec; ?>%
                          </div>
                      </div>
                   </p>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
              <div class="col-sm-8">
                  <div class="box">
                      <div class="box-header with-border">
                           <h3 class="box-title"><b style="font-size: 30px; font-family:Helvetica;">Overall Summary</b></h3><br>
                      </div>
                      <!-- /.box-header -->
                            <div class="box-body" style="margin-top: -30px;">
                                  <div class="row">
                                      <div class="col-lg-4">
                                        <div id="smallbuddy" style="width:230px; height:130px">
                                        <div id="gauge" class="200x160px"></div>
                                        </div>
                                      </div>
                                      
                                      <div class="col-lg-4">
                                        <div id="smallbuddy" style="width:230px; height:130px">
                                        <div id="gauge1" class="200x160px"></div>
                                        </div>
                                      </div>
                                      
                                      <div class="col-lg-4">
                                        <div id="smallbuddy" style="width:230px; height:130px">
                                        <div id="gauge2" class="200x160px"></div>
                                        </div><br>
                                      </div>
                                      <div class="col-sm-12" style="width:710px; margin:0 auto;">
                                     <div class="box-header with-border">
										   <h3 class="box-title"><b style="font-size: 30px; font-family:Helvetica;">&nbsp;Job Card List</b></h3>
									  </div>
                                       <table id="example_jc" class="table table-bordered table-striped" style="width: 100%;"><br>
                                        <thead>
                                          <tr>
                                            <th>NO</th>
                                            <th>MAT</th>
                                            <th>DESCRIPTION</th>
                                            <th>STATUS</th>
                                          </tr>
                                        </thead>
                                        <tbody>
											  <?php
											  $no = 0;
											  if (is_array($listJobCard)) {
											   foreach ($listJobCard as $row) {
												$no++;
												 ?>
												<tr>
												  <td><?php echo $no; ?></td>
												  <td><?php echo $row->ILART; ?></td>
												  <td><?php echo $row->KTEXT; ?></td>
												  <td id="color_status_<?php echo $no; ?>"><input type="hidden" id="status_<?php echo $no; ?>" value="<?php echo $row->STATUS; ?>"/><?php echo $row->STATUS; ?></td>
												</tr>
											  <?php }} ?>
											</tbody>
                                      </table>   
                                      </div>    
                        </div>
                      </div> 
                    <!-- /.box -->
                  </div>
              </div>



                            </div>
                      <!-- /.box-body -->
                          </div>
                    <!-- /.box -->
                         </div>

</section>
<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/justgage.js/justgage.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/justgage.js/raphael-2.1.4.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
	  
	   //td color
		  for (var i=1; i<=<?php echo $no; ?>; i++){
			var status_ =  document.getElementById('status_'+i).value;
			
			if(status_ == "OPEN"){
			  document.getElementById('color_status_'+i).style.backgroundColor='#42a5f6';
			}else if(status_ == "PROGRESS"){
			  document.getElementById('color_status_'+i).style.backgroundColor='#ff5622';
			}else if(status_ == "CLOSED"){
			  document.getElementById('color_status_'+i).style.backgroundColor='#38e144';
			}
		  }
  
  
   // DataTable
        var table = $('#example_jc').DataTable({
            scrollY: "500px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 15,
            ordering: true,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>

<script type="text/javascript">
    var g5 = new JustGage({
    id: "gauge",
    value:<?php echo $percentage_jc_open; ?>,
    min: 0,
    max: 100,
    symbol: '%',
    levelColors: ["#42A5F5"],
    label: "Open Job Cards",
    levelColorsGradient: false,
  });

  var g = new JustGage({
    id: "gauge1",
    value:<?php echo $percentage_jc_progress; ?>,
    min: 0,
    max: 100,
    symbol: '%',
    levelColors: ["#FF5722"],
    label: "In Progress Job Cards"
  });

  var g = new JustGage({
    id: "gauge2",
    value: <?php echo $percentage_jc_closed; ?>,
    min: 0,
    max: 100,
    symbol: '%',
    levelColors: ["#38e144"],
    label: "Closed Job Cards"
  });
</script>



