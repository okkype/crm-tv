

<section class="content">
<?php 
$this->load->view($header_menu); 
$this->load->view($title_menu); 
?>

<div class="row">
        <div class="col-md-12">
          <!-- Line chart -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

          <!--     <h3 class="box-title">Line Chart</h3> -->

             <!--  <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div> -->
            <div style="width:45%; margin:0 auto;">
              <canvas id="bar-chart-horizontal" width="800" height="450"></canvas>
            </div>
            <div class="col-sm-12" style="width:1065px; margin:0 auto;">  
             <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                            <th>Dev No</th>
                            <th>TAT</th>
                            <th>Gate</th>
                            <th>Deviation Details</th>
                            <th>Root Cause</th>
                            <th>Corrective Action</th>
                            <th>Created By</th>
                            <th>Created Date</th>
                          </tr>
                      </thead>

                      <tbody>
                          <?php foreach($deviation as $row): ?>
                        <tr>
                          <td><?php echo $row->DEV_NUMBER ?></td>
                          <td><?php echo $row->DELAY_TAT ?></td>
                          <td><?php echo $row->CURRENT_GATE ?></td>
                          <td><?php echo $row->DEVIATION_REASON ?></td>
                          <td><?php echo $row->ROOT_CAUSE ?></td>
                          <td><?php echo $row->CORECTIVE_ACTION ?></td>
                          <td><?php echo $row->CREATED_BY ?></td>
                          <td><?php echo $row->CREATED_DATE ?></td>
                        </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                 </div>
            <!-- /.box-body-->

          </div>
          <!-- /.box -->
        </div>

    </div>
</div>
</section>

<script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.min.js"></script>
<script type="text/javascript">
new Chart(document.getElementById("bar-chart-horizontal"), {
    type: 'horizontalBar',
    data: {
      labels: ["GATE 1", "GATE 2", "GATE 3", "GATE 4", "GATE 5"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#b8525d","#f6f29e","#e1bd52","#6fa17a","#0ea685"],
          data: [2478,2132,5267,734,784]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Deviation Total'
      }
    }
});
</script>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
   // DataTable
        var table = $('#example').DataTable({
            scrollY:        "500px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: false,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>