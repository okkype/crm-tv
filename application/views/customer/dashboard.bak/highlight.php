<style type="text/css">
.content {
        font-family: "segoeui";
        src: url("assets/bower_components/font-awesome/fonts/segoeui.ttf");
    }
</style>

<section class="content">
<?php 
$this->load->view($header_menu); 
$this->load->view($title_menu); 
?>

<div class="row">
        <div class="col-md-12">
          <!-- Line chart -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

          <!--     <h3 class="box-title">Line Chart</h3> -->

              <!-- <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div> -->
            <div style="width:50%; margin:0 auto;">
              <canvas id="bar-chart-horizontal" width="800" height="450"></canvas>
            </div>

            <div class="col-sm-12">   
                   <br>       
                    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <tr>
                           <th>NO</th>
                           <th>HL NO</th>
                           <th>Applicable Module</th>
                           <th>Highlight</th>
                           <th>Status</th>
                           <th>Created By</th>
                           <th>Created Date</th>
                          </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>7000000</td>
                          <td>Power Section</td>
                          <td>Perform SB 49-8026 remove dulpex ball bearing</td>
                          <td>Closed</td>
                          <td>G529560</td>
                          <td>01.032017</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>7000001</td>
                          <td>Fan Module</td>
                          <td>Fanblade robbery ESN 725614 to ESN 727372</td>
                          <td>Open</td>
                          <td>G529560</td>
                          <td>06.03.2017</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>7000002</td>
                          <td>Coe Major Module</td>
                          <td>Component robbery ESN 725614 to ESN 727372 Par HMU</td>
                          <td>Closed</td>
                          <td>G529560</td>
                          <td>03.03.2017</td>
                        </tr>
                      </tbody>
                    </table>
                  <!-- </div> -->
                  </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->


        </div>

      </div>

</div>

</section>

<script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.min.js"></script>
<script type="text/javascript">
new Chart(document.getElementById("bar-chart-horizontal"), {
    type: 'horizontalBar',
    data: {
      labels: ["Open", "Close"],
      datasets: [
        {
          label: "Status",
          backgroundColor: ["#b8525d","#0da785"],
          data: [2478,2350]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Status'
      }
    }
});


 $(document).ready(function() {
   // DataTable
        var table = $('#example').DataTable({
            scrollY:        "500px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>