<section class="content">
    <?php
    $this->load->view($header_menu);
    $this->load->view($title_menu);
    ?>
    
    <div class="content-header">
        <h1 align="center">
            EO REPORT
        </h1>
        <p align="center">_____________________________</p>
        <h4 align="center">Project ESN : <?php echo $report->ESN; ?></h4>
    </div>
    
    <?php echo $report->CONTENT; ?>

</section>

<script src="<?php echo base_url(); ?>assets/plugins/tinymce/tinymce.min.js">
</script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        height: 400,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
    });
</script>