<!-- <?php //list($REVNR,$ESN,$EO,$WORKSCOPE,$CONTRACTUAL_TAT,$INDUCTION_DATE,$CURRENT_TAT,$COMPANY_NAME,$REVBD,$REVED,$TYPE) = $listProject; ?> -->
<div class="container-fluid">
<br>
<h4 align="center"><u><?php echo $title; ?></u></br><?php echo $ESN; ?></h4>

<div class="row">
	<div class="col-lg-12 col-xs-12">
	  <div class="box box-solid">
		  <div class="box">
			  <div class="container-fluid" >
			  <div class="col-lg-12 col-xs-2 ">
				  <table  class="stripe row-border" width="100%">
				  	<col width="10%">
  					<col width="5%">
  					<col width="45%">
  					<col width="10%">
  					<col width="5%">
  					<col width="25%">
					<tbody>
						<tr>
							<td><b>Engine Owner</b></td>
							<td>:</td>
							<td><?php echo $TEAM_EO; ?></td>
							<td><b>Customer</b></td>
							<td>:</td>
							<td></td>
						</tr>
						<tr>
							<td><b>Workscope</b></td>
							<td>:</td>
							<td style="text-align: top center;"><?php echo $WORKSCOPE; ?></td>
							<td><b>Type</b></td>
							<td>:</td>
							<td><?php echo $ATWRT; ?> ( <?php echo $ENGINE_APU; ?> )</td>
						</tr>
						<tr>
							<td><b>Induction Date</b></td>
							<td>:</td>
							<td><?php echo $INDUCT_DATE; ?></td>
							<td><b>Contractual TAT</b></td>
							<td>:</td>
							<td><?php echo $CONTRACTUAL_TAT; ?></td>
						</tr>
					</tbody>
				</table>
				</div>
				 </div>
				</div>
		  </div>
	  </div>
	</div>
</div>
