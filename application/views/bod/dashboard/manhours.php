<section class="content">
<?php
$this->load->view($header_menu);
$this->load->view($title_menu);
?>
<div class="container-fluid">
<div class="row">
  <div class="col-md-12">
    <!-- Line chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
      <!--   <i class="fa fa-bar-chart-o"></i> -->

    <!--     <h3 class="box-title">Line Chart</h3> -->

        <!-- <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div> -->

      <div class="col-md-4">
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Total</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body chart-responsive">
            <div class="chart" id="bar-chart" style="height: 200px;">
              <canvas id="bar-chart-grouped" width="800" height="500"></canvas>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Job Card</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 200px;">
                <canvas id="bar-chart-grouped1" width="800" height="500"></canvas>
              </div>
            </div>
          </div>
      </div>

      <div class="col-md-4">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">MDR</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 200px;">
                  <canvas id="bar-chart-grouped2" width="800" height="500"></canvas>
              </div>
            </div>
          </div>
      </div>

      <div class="col-sm-12" style="background-color:white;">
                  <!-- #f4f1f4 -->
                   <div class="col-sm-12 table-responsive">
                    <table id="tv" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Seq</th>
                          <th>Order Number</th>
                          <th>Order Type</th>
                          <th>MAT</th>
                          <th>Description</th>
                          <th>Status</th>
                          <th>Mhrs Plan (Hrs)</th>
                          <th>Mhrs Actual (Hrs)</th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php
                        $no = 0;
                        if (is_array($listManhour)) {
                         foreach ($listManhour as $row) {
                        $no++;
                         ?>
                         <tr>
                           <td><?= $no ?></td>
                           <td><?= $row->AUFNR ?></td>
                           <td><?= $row->AUART ?></td>
                           <td><?= $row->MAT ?></td>
                           <td><?= $row->KTEXT ?></td>
                           <td>KOSONG</td>
                           <td><?= $row->ARBEI ?></td>
                           <td><?= $row->ISMNW ?></td>
                         </tr>


                        <?php }} ?>
                      </tbody>
                    </table>
                  <!-- </div> -->
                  </div>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
</section>


<script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.min.js"></script>
<script type="text/javascript">
new Chart(document.getElementById("bar-chart-grouped"), {
    type: 'bar',
    data: {
      labels: ["PLAN", "ACTUAL"],
      datasets: [
        {
          label: "Status",
          backgroundColor: ["#fff15e","#3a7ce8"],
          data: <?= $chart['total'] ?>
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
<script type="text/javascript">
  new Chart(document.getElementById("bar-chart-grouped1"), {
    type: 'bar',
    data: {
      labels: ["JOBCARD", "ACTUAL"],
      datasets: [
        {
          label: "Status",
          backgroundColor: ["#b8525d","#3a7ce8"],
          data: <?= $chart['jobcard'] ?>
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
<script type="text/javascript">
  new Chart(document.getElementById("bar-chart-grouped2"), {
    type: 'bar',
    data: {
      labels: ["MDR", "ACTUAL"],
      datasets: [
        {
          label: "Status",
          backgroundColor: ["#5def64","#3a7ce8"],
          data: <?= $chart['mdr'] ?>
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
<script type="text/javascript">
  $(document).ready(function() {

  // DataTable
        var table = $('#tv').DataTable({
            // scrollY: "200px",
            // dom: 'Bfrtip',
            dom: 'Blfrtip',
        // lengthMenu: [
        //     [ 10, 25, 50, -1 ],
        //     [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        // ],
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: true
        });
} );
</script>


