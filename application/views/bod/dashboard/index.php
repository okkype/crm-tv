<style type="text/css">
    .content-header {
        font-family: "Helvetica";
        src: url("assets/bower_components/font-awesome/fonts/HelveticaLTStd-Roman_0.otf");
    }

    .content {
        font-family: "Helvetica";
        src: url("assets/bower_components/font-awesome/fonts/HelveticaLTStd-Roman_0.otf");
    }
</style>

<section class="content-header">
    <div class="panel panel-default">
        <h1 align="center">
            <u>Project List</u><br>
            Engine Maintenance
        </h1>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class=" table-responsive ">
                        <table id="example" class="table table-responsive table-bordered table-striped" style="width: 100%">
                            <thead>
                            <th>No.</th>
                            <th>ESN</th>
                            <th>Project No</th>
                            <th>EO</th>
                            <th>CUSTOMER</th>
                            <th>WORKSCOPE</th>
                            <th>INDUCTION DATE</th>
                            <th>CONTRACTUAL TAT</th>
                            <th>CURRENT TAT</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($listProject) {
                                    $no = 0;
                                    foreach ($listProject as $row) {
                                        $no++;
                                        ?>  
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td>
                                                <a href= "<?php echo base_url(); ?>index.php/administrator/dashboard/overview/<?php echo $row['EQUNR']; ?>/<?php echo $row['REVNR']; ?>" href='javascript:void(0);'><?php echo $row['SERNR']; ?></a>
                                            </td> 
                                            <td><?php echo $row['REVNR']; ?></td>             
                                            <td><?php echo $row['TEAM_EO']; ?></td>
                                            <td><?php echo $row['COMPANY_NAME']; ?></td>
                                            <td><?php echo $row['WORKSCOPE']; ?></td>
                                            <td><?php
                                                $source = $row['INDUCT_DATE'];
                                                $date = new DateTime($source);
                                                echo $date->format('d-M-Y'); // 31-07-2012 
                                                ?></td>
                                            <td >
                                                <input type="hidden" id="start_<?php echo $no; ?>" value="<?php echo $row['CONTRACTUAL_TAT']; ?>"/><?php echo $row['CONTRACTUAL_TAT']; ?> Days
                                            </td>
                                            <td id="color_curtat_<?php echo $no; ?>"><input type="hidden" id="end_<?php echo $no; ?>" value="<?php echo $row['CURRENT_TAT']; ?>"/><?php echo $row['CURRENT_TAT']; ?> Days
                                            </td>
                                        </tr>
    <?php }
} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
    </div>

</section>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        //td color
        for (var i = 1; i <=<?php echo $no; ?>; i++) {
            var start_ = document.getElementById('start_' + i).value;
            var end_ = document.getElementById('end_' + i).value;
            if (end_ - start_ > 0) {
                document.getElementById('color_curtat_' + i).style.backgroundColor = '#f1c1c0';
            } else {
                document.getElementById('color_curtat_' + i).style.backgroundColor = '#dbecc6';
            }
        }


        // DataTable
        // var table = $('#example').DataTable({
        //     scrollY:        "500px",
        //     dom: 'Bfrtip',
        //     scrollX: true,
        //     scrollCollapse: true,
        //     paging: true,
        //     fixedColumns: true,
        //     pageLength: 10,
        //     ordering: true,
        //     buttons: [
        //         //{
        //         //extend: "pageLength",
        //         //className: "btn btn-default"
        //         //}
        //     ],
        // });
        var table = $('#example').DataTable({
            dom: 'Blfrtip',
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            // buttons: [
            //            {
            //                extend: 'excelHtml5',
            //                extension: '.xlsx',
            //                text: 'Excel'
            //            }    
            //         ],
        });
    });
</script>
