<style type="text/css">
.content {
        font-family: "segoeui";
        src: url("assets/bower_components/font-awesome/fonts/segoeui.ttf");
    }
</style>

<section class="content">
<?php 
$this->load->view($header_menu); 
$this->load->view($title_menu); 
?>

<div class="row">
        <div class="col-md-12">
          <!-- Line chart -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

          <!--     <h3 class="box-title">Line Chart</h3> -->

             <!--  <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div> -->
            <div style="width:45%; margin:0 auto;">
              <canvas id="bar-chart-horizontal" width="800" height="450"></canvas>
            </div>
            <div class="col-sm-12" style="width:1065px; margin:0 auto;">  
             <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                            <th rowspan="2">No.</th>
                            <th rowspan="2">Dev No</th>
                            <th colspan="3">TAT</th>
                            <th colspan="2">Gate</th>
                            <th colspan="3">Deviation Details</th>
                            <th rowspan="2">Root Cause</th>
                            <th rowspan="2">Corrective Action</th>
                            <th rowspan="2">Created By</th>
                            <th rowspan="3">Created Date</th>
                          </tr>
                          <tr>
                            <th>Eng TAT</th>
                            <th>Cus TAT</th>
                            <th>Prc TAT Increase</th>
                            <th>Current Gate</th>
                            <th>Next Gate Eff</th>
                            <th>Dev Reason</th>
                            <th>Prc Delayed</th>
                            <th>Department</th>
							
                          </tr>
                      </thead>

                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>2000003</td>
                          <td>4</td>
                          <td>0</td>
                          <td>3</td>
                          <td>3</td>
                          <td>4</td>
                          <td>CAPACITY</td>
                          <td>G3-BENCH INSP</td>
                          <td>BENCH INSP</td>
                          <td>Adanya antrian proses inspeksi untuk APU WIP karena kekurangan man power</td>
                          <td>Overtime</td>
                          <td>G531965</td>
                          <td>31-Oct-17</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>2000009</td>
                          <td>4</td>
                          <td>0</td>
                          <td>3</td>
                          <td>3</td>
                          <td>4</td>
                          <td>CAPACITY</td>
                          <td>G3-BENCH INSP</td>
                          <td>BENCH INSP</td>
                          <td>Inspection process is queing due to less of man power</td>
                          <td>Overtime</td>
                          <td>G531965</td>
                          <td>22-Nov-17</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>2000013</td>
                          <td>1</td>
                          <td>0</td>
                          <td>8</td>
                          <td>8</td>
                          <td>9</td>
                          <td>TROUBLE PROCESS</td>
                          <td>G8-TEST</td>
                          <td>TEST CELL</td>
                          <td>Test cell process problem due to SCV fault message</td>
                          <td>Part is sent to TC for warranty claim & request for approval for replacement using spare GA</td>
                          <td>G531965</td>
                          <td>22-Nov-17</td>
                        </tr>
                        <tr>
                          <td>4</td>
                          <td>2000011</td>
                          <td>18</td>
                          <td>0</td>
                          <td>5</td>
                          <td>5</td>
                          <td>6</td>
                          <td>MATERIAL</td>
                          <td>G5-OV REPAIR</td>
                          <td>PURCHASER</td>
                          <td>Long time TAT repair for turbine 2nd stg</td>
                          <td>RFQ & Issued PO replacement</td>
                          <td>G531965</td>
                          <td>31-Oct-17</td>
                        </tr>
                      </tbody>
                    </table>
                 </div>
            <!-- /.box-body-->

          </div>
          <!-- /.box -->
        </div>

    </div>
</div>
</section>

<script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.min.js"></script>
<script type="text/javascript">
new Chart(document.getElementById("bar-chart-horizontal"), {
    type: 'horizontalBar',
    data: {
      labels: ["GATE 1", "GATE 2", "GATE 3", "GATE 4", "GATE 5"],
      datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["#b8525d","#f6f29e","#e1bd52","#6fa17a","#0ea685"],
          data: [2478,2132,5267,734,784]
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Deviation Total'
      }
    }
});
</script>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
   // DataTable
        var table = $('#example').DataTable({
            scrollY:        "500px",
            dom: 'Bfrtip',
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            pageLength: 10,
            ordering: false,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>