<section class="content">
<?php 
$this->load->view($header_menu); 
$this->load->view($title_menu); 
?>

<div class="row">
        <div class="col-md-12">
          <!-- Line chart -->
          <div class="box box-primary">
            <div class="box-header with-border">
            <!--   <i class="fa fa-bar-chart-o"></i> -->

          <!--     <h3 class="box-title">Line Chart</h3> -->

              <!-- <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div> -->
           
                    <div style="width:1000px; margin:0 auto;">
        <div class="col-md-4">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Total</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 200px;">
                <canvas id="bar-chart-grouped" width="800" height="500"></canvas>
              </div>
            </div>
          </div>
        </div>
    
    <div class="col-md-4">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Job Card</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 200px;">
                <canvas id="bar-chart-grouped1" width="800" height="500"></canvas>
              </div>
            </div>
          </div>
        </div>
    
    <div class="col-md-4">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">MDR</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bar-chart" style="height: 200px;">
                  <canvas id="bar-chart-grouped2" width="800" height="500"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-12" style="background-color:white;">
                  <!-- #f4f1f4 -->
                   <div class="col-sm-12">          
                    <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>Seq</th>
                          <th>Order Number</th>
                          <th>Order Type</th>
                          <th>MAT</th>
                          <th>Description</th>
                          <th>Status</th>
                          <th>Mhrs Plan (Hrs)</th>
                          <th>Mhrs Actual (Hrs)</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1.1</td>
                          <td>800000131</td>
                          <td>GA02</td>
                          <td>MDR</td>
                          <td>Fuel Filter Removal</td>
                          <td>REL CNF</td>
                          <td>0.5</td>
                          <td>0.5</td>
                        </tr>
                        <tr>
                          <td>1.1</td>
                          <td>800000132</td>
                          <td>GA01</td>
                          <td></td>
                          <td>Modification in Shut Off Valve</td>
                          <td>CLSD</td>
                          <td>10.0</td>
                          <td>0.0</td>
                        </tr>
                        <tr>
                          <td>2.1</td>
                          <td>800000124</td>
                          <td>GA01</td>
                          <td></td>
                          <td>1A0020 Removals</td>
                          <td>REL CNF</td>
                          <td>120.0</td>
                          <td>120.0</td>
                        </tr>
                        <tr>
                          <td>2.1</td>
                          <td>800000130</td>
                          <td>GA02</td>
                          <td>MDR</td>
                          <td>CI Replacement</td>
                          <td>CLSD</td>
                          <td>0.0</td>
                          <td>0.0</td>
                        </tr>
                        <tr>
                          <td>3.1</td>
                          <td>800000125</td>
                          <td>GA01</td>
                          <td></td>
                          <td>1A0030 Cleaning</td>
                          <td>CLSD</td>
                          <td>120.0</td>
                          <td>0.0</td>
                        </tr>
                        <tr>
                          <td>3.1</td>
                          <td>800000129</td>
                          <td>GA02</td>
                          <td>MDR</td>
                          <td>Defect description</td>
                          <td>REL CNF</td>
                          <td>10.0</td>
                          <td>12.0</td>
                        </tr>
                        <tr>
                          <td>4.1</td>
                          <td>800000126</td>
                          <td>GA01</td>
                          <td></td>
                          <td>Aerofoil InitialF/chks</td>
                          <td>CLSD</td>
                          <td>24.0</td>
                          <td>30.0</td>
                        </tr>
                        <tr>
                          <td>4.1</td>
                          <td>800000140</td>
                          <td>GA02</td>
                          <td>MDR</td>
                          <td>Defect in Hydraulic Pump</td>
                          <td>REL PCNF</td>
                          <td>12.0</td>
                          <td>10.0</td>
                        </tr>
                        <tr>
                          <td>5.1</td>
                          <td>8000000127</td>
                          <td>GA01</td>
                          <td></td>
                          <td>Aerofoil 1F0020 Cleaning</td>
                          <td>CLSD</td>
                          <td>8.0</td>
                          <td>0.0</td>
                        </tr>
                      </tbody>
                    </table>
                  <!-- </div> -->
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
          <br>
     

</section>


<script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.min.js"></script>
<script type="text/javascript">
new Chart(document.getElementById("bar-chart-grouped"), {
    type: 'bar',
    data: {
      labels: ["1900"],
      datasets: [
        {
          label: "MDR",
          backgroundColor: "#1f78b4",
          data: [520,300]
        }, {
          label: "ACTUAL",
          backgroundColor: "#fe7f0e",
          data: [408,547]
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
<script type="text/javascript">
  new Chart(document.getElementById("bar-chart-grouped1"), {
    type: 'bar',
    data: {
      labels: ["1900"],
      datasets: [
        {
          label: "MDR",
          backgroundColor: "#1f78b4",
          data: [720,300]
        }, {
          label: "ACTUAL",
          backgroundColor: "#fe7f0e",
          data: [408,547]
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
<script type="text/javascript">
  new Chart(document.getElementById("bar-chart-grouped2"), {
    type: 'bar',
    data: {
      labels: ["1900"],
      datasets: [
        {
          label: "MDR",
          backgroundColor: "#1f78b4",
          data: [408,547]
        }, {
          label: "ACTUAL",
          backgroundColor: "#fe7f0e",
          data: [720,300]
        }
      ]
    },
    options: {
      title: {
        display: true,
      }
    }
});
</script>
</script>

           

            <!-- /.box-body-->
          </div>
          <!-- /.box -->


        </div>

      </div>

</div>
</section>