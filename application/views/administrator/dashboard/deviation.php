<!-- <style type="text/css">
.content {
        font-family: "segoeui";
        src: url("assets/bower_components/font-awesome/fonts/segoeui.ttf");
    }
</style> -->

<section class="content">
<?php
$this->load->view($header_menu);
$this->load->view($title_menu);
?>
<div class="container-fluid">
<div class="row">
        <div class="col-md-12">
          <!-- Line chart -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

          <!--     <h3 class="box-title">Line Chart</h3> -->

             <!--  <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div> -->
            <div style="width:45%; margin:0 auto;">
              <canvas id="bar-chart-horizontal" width="800" height="450"></canvas>
            </div>
            <div class="col-sm-12" style="width:1065px; margin:0 auto;">
             <table id="tbdevpoint" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                            <th rowspan="2">No.</th>
                            <th rowspan="2">Dev No</th>
                            <th colspan="3">TAT</th>
                            <th colspan="2">Gate</th>
                            <th colspan="3">Deviation Details</th>
                            <th rowspan="2">Root Cause</th>
                            <th rowspan="2">Corrective Action</th>
                            <th rowspan="2">Created By</th>
                            <th rowspan="3">Created Date</th>
                          </tr>
                          <tr>
                            <th>Eng TAT</th>
                            <th>Cus TAT</th>
                            <th>Prc TAT Increase</th>
                            <th>Current Gate</th>
                            <th>Next Gate Eff</th>
                            <th>Dev Reason</th>
                            <th>Prc Delayed</th>
                            <th>Department</th>

                          </tr>
                      </thead>

                      <tbody>
                        <?php
                        $no = 0;
                        if (is_array($listDeviation)) {
                         foreach ($listDeviation as $row) {
                        $no++;
                         ?>
                         <tr>
                           <td><?= $no ?></td>
                           <td><?= $row->DEV_NUMBER ?></td>
                           <td><?= $row->DELAY_TAT ?></td>
                           <td><?= $row->DELAY_CUSTOMER ?></td>
                           <td><?= $row->CG_DELAY_PROCESS ?></td>
                           <td><?= $row->CURRENT_GATE ?></td>
                           <td><?= $row->NEXT_GATE_EFFCTD ?></td>
                           <td><?= $row->DEVIATION_REASON ?></td>
                           <td><?= $row->PROCESS_DELAYED ?></td>
                           <td><?= $row->DEPARTEMENT ?></td>
                           <td><?= $row->ROOT_CAUSE ?></td>
                           <td><?= $row->CORECTIVE_ACTION ?></td>
                           <td><?= $row->CREATED_BY ?></td>
                           <td><?= $row->CREATED_DATE ?></td>
                         </tr>

                        <?php }} ?>

                      </tbody>
                    </table>
                 </div>
            <!-- /.box-body-->

          </div>
          <!-- /.box -->
        </div>

    </div>
</div>
</div>
</section>

<script src="<?php echo base_url(); ?>assets/bower_components/Chart.js/Chart.min.js"></script>
<script type="text/javascript">
var color = Chart.helpers.color;
new Chart(document.getElementById("bar-chart-horizontal"), {
    type: 'horizontalBar',
    data: {
      labels: ["GATE 1", "GATE 2", "GATE 3", "GATE 4", "GATE 5", "GATE 6", "GATE 7", "GATE 8", "GATE 9"],
      datasets: [
        {
          label: "Population (millions)",
          // backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
          // borderColor: window.chartColors.red,
          // backgroundColor: ["#b8525d","#f6f29e","#e1bd52","#6fa17a","#0ea685","#f3429e","#e1bc82","#6fb97a","#24a685"],
          backgroundColor:  ['#ccebff','#ffcccc','#ffd1b3','#ffeb99','#e6ccff','#e6fffa','#e0e0d1','#b3ffb3','##c2d6d6'],
          borderColor:['#66c2ff','#ff6666','#ff751a','#e6b800','#a64dff','#adad85','#1affd1','#4dff4d','#527a7a'],
          borderWidth : 1,
          hoverBackgroundColor:['#66c2ff','#ff6666','#ff751a','#e6b800','#a64dff','#adad85','#1affd1','#4dff4d','#527a7a'],
          hoverBorderColor :['#ccebff','#ffcccc','#ffd1b3','#ffeb99','#e6ccff','#e6fffa','#e0e0d1','#b3ffb3','##c2d6d6'],
          data: <?= $count_gate ?>
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Deviation Total'
      }
    }
});
</script>

<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
   // DataTable
        var table = $('#tbdeviation').DataTable({
            scrollY:        "500px",
            dom:            'Bfrtip',
            scrollX:        true,
            scrollCollapse: true,
            paging:         true,
            fixedColumns:   true,
            pageLength:     10,
            ordering:       false,
            buttons: [
                //{
                //extend: "pageLength",
                //className: "btn btn-default"
                //}
            ],
        });
} );
</script>
