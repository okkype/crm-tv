<?php 
    function menu_link($param){
        return base_url() . $param;
    }
?>

<ul class="nav nav-tabs">
    <li <?php echo ($content == 'admin/user_management/crud_users') ? 'class="active"' : '' ?>>
        <a href="<?php echo menu_link('admin/user_management/crud_users') ?>">User</a>
    </li>
    <li <?php echo ($content == 'admin/user_management/user_group') ? 'class="active"' : '' ?>>
        <a href="<?php echo menu_link('admin/user_management/user_group') ?>">User Group</a>
    </li>
    <li <?php echo ($content == 'admin/user_management/work_area') ? 'class="active"' : '' ?>>
        <a href="<?php echo menu_link('admin/user_management/work_area'); ?>">Work Area</a>
    </li>
    <li <?php echo ($content == 'admin/user_management/menu_management') ? 'class="active"' : '' ?>>
        <a href="<?php echo menu_link('admin/user_management/menu_management'); ?>">Menu Management</a>
    </li>                    
</ul>