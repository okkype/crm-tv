<div id="modalAddWorkArea" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="form-user-title"></h4>
        </div>
        <div class="modal-body">
            <div class="form-horizontal">
                <div id="msg" class="alert">
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="code">CODE</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="code" placeholder="Enter code of Work Area">
                    </div>
                </div>           
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">NAME</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" placeholder="Enter Work Area">
                    </div>
                </div>                                                                                               
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btn-save">Save</button>
        </div>
    </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#msg').hide();
        $('#btn-save').on("click", function(){
            var btn = $('#btn-save').html();
            var url_location = '';
            if(btn === 'Save'){
                url_location = '<?php echo base_url() ?>' + 'admin/user_management/add_new_work_area';
            }else{
                url_location = '<?php echo base_url() ?>' + 'admin/user_management/update_work_area';
            }
            
            var data = {   
                        id: $('#code').val(),
                        name: $('#name').val()
                    }

            $.ajax({
                type: "POST",
                url: url_location,
                data: data,
                dataType: "text",
                cache: false,
                success: function(data){
                    var msg = $.parseJSON(data);
                    if(msg.status === 'failed'){
                        swal(msg.body_msg, {
                                icon: "error"
                        });
                    }else{
                        swal(msg.body_msg, {
                                icon: "success"
                        });
                        datagrid.fetchGrid();
                    }
                }
            }); 
        })
    })
</script>