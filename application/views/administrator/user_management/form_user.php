<div id="modalAddUser" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="form-user-title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div id="msg" class="alert">
                    </div>
                    <input type="text" class="form-control" id="user_id"  style="display:none">             
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name">Fullname</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" placeholder="Enter Fullname">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="username">Username</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="username" placeholder="Enter Username">
                        </div>
                    </div>                              
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Password</label>
                        <div class="col-sm-10"> 
                            <input type="password" class="form-control" id="pwd" placeholder="Enter password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="access">Access</label>
                        <div class="col-sm-10">
                            <select id="access" class="form-control">
                                <?php foreach ($user_group as $value) { ?>
                                    <option value="<?php echo $value->ID_USER_GROUP; ?>"><?php echo $value->USER_GROUP ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="customer">Customer</label>
                        <div class="col-sm-10">
                            <select id="customer" name="customer" class="form-control">
                                <?php foreach ($customer as $value) { ?>
                                    <option value="<?php echo $value->KUNNR; ?>"><?php echo $value->NAME1 ?></option>
                                <?php } ?>
                            </select>                    
                        </div>
                    </div>
                    <!--                <div class="form-group">
                                        <label class="control-label col-sm-2" for="work_area">Work Area</label>
                                        <div class="col-sm-10">
                                            <select id="work_area" class="form-control">
                    <?php foreach ($work_area as $value) { ?>
                                                            <option value="<?php echo $value->ID_WORK_AREA; ?>"><?php echo $value->WORK_AREA ?></option>
                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>                                                                     -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-save">Save</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#customer').select2();
        $('.select2').css('width', '100%');
        $('#msg').hide();
        $('#btn-save').on("click", function () {
            var btn = $('#btn-save').html();
            var url_location = '';
            if (btn === 'Save') {
                url_location = '<?php echo base_url() ?>' + 'index.php/administrator/user_management/add_new_users';
            } else {
                url_location = '<?php echo base_url() ?>' + 'index.php/administrator/user_management/update_user';
            }

            var data = {
                id: $('#user_id').val(),
                name: $('#name').val(),
                username: $('#username').val(),
                password: $('#pwd').val(),
                group_id: $('#access').val(),
                customer_id: $('#customer').val(),
                work_area_id: $('#work_area').val()};

            $.ajax({
                type: "POST",
                url: url_location,
                data: data,
                dataType: "text",
                cache: false,
                success: function (data) {
                    var msg = $.parseJSON(data);
                    if (msg.status === 'failed') {
                        swal("Error saving Record!", {
                            icon: "error"
                        });
                    } else {
                        swal("User has been saved successfully!", {
                            icon: "success"
                        });
                        datagrid.fetchGrid();
                    }
                }
            });
        })
    })
</script>