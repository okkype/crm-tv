<!-- Main content -->
<?php $this->load->view('administrator/user_management/form_user'); ?>
<?php $this->load->view('administrator/user_management/header'); ?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <!-- ?php $this->load->view('administrator/user_management/user_menu'); ? -->
                <div class="tab-content">
                    <section class="content" style="overflow: auto;">
                        <div id="message"></div>
                        <div id="wrap">
                            <!-- Feedback message zone -->
                            <div id="toolbar">
                                <input type="text" id="filter" name="filter" placeholder="Filter :type any text here"  />
                                <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalAddUser" id="btnCreateNew"> 
                                    <i class="fa fa-plus-circle">
                                    </i> New User
                                </button>
                            </div>
                            <!-- Grid contents -->
                            <div id="tablecontent"></div>
                            <!-- Paginator control -->
                            <div id="paginator"></div>
                        </div>
                        <!-- script src="<?php echo base_url(); ?>assets/editablegrid/js/jquery-1.11.1.min.js" ></script -->
                        <script src="<?php echo base_url(); ?>assets/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
                        <script type="text/javascript">
                            function highlightRow(rowId, bgColor, after)
                            {
                                var rowSelector = $("#" + rowId);
                                rowSelector.css("background-color", bgColor);
                                rowSelector.fadeTo("normal", 0.5, function () {
                                    rowSelector.fadeTo("fast", 1, function () {
                                        rowSelector.css("background-color", '');
                                    });
                                });
                            }

                            function highlight(div_id, style) {
                                highlightRow(div_id, style == "error" ? "#e5afaf" : style == "warning" ? "#ffcc00" : "#8dc70a");
                            }

                            function message(type, message) {
                                $('#message').html("<div class=\"notification  " + type + "\">" + message + "</div>").slideDown('normal').delay(1800).slideToggle('slow');
                            }

                            /**
                             updateCellValue calls the PHP script that will update the database.
                             */
                            function updateCellValue(editableGrid, rowIndex, columnIndex, oldValue, newValue, row, onResponse)
                            {
                                $.ajax({
                                    url: '<?php echo base_url(); ?>administrator/projects/view_vcustomer_update',
                                    type: 'POST',
                                    dataType: "html",
                                    data: {
                                        tablename: editableGrid.name,
                                        id: editableGrid.getRowId(rowIndex),
                                        newvalue: editableGrid.getColumnType(columnIndex) == "boolean" ? (newValue ? 1 : 0) : newValue,
                                        colname: editableGrid.getColumnName(columnIndex),
                                        coltype: editableGrid.getColumnType(columnIndex)
                                    },
                                    success: function (response)
                                    {
                                        // reset old value if failed then highlight row
                                        var success = onResponse ? onResponse(response) : (response == "ok" || !isNaN(parseInt(response))); // by default, a sucessfull reponse can be "ok" or a database id
                                        if (!success)
                                            editableGrid.setValueAt(rowIndex, columnIndex, oldValue);
                                        highlight(row.id, success ? "ok" : "error");
                                    },
                                    error: function (XMLHttpRequest, textStatus, exception) {
                                        alert("Ajax failure\n" + errortext);
                                    },
                                    async: true
                                });

                            }

                            function DatabaseGrid()
                            {
                                this.editableGrid = new EditableGrid("vcustomer", {
                                    editmode: 'static',
                                    enableSort: true,

                                    /* Comment this line if you set serverSide to true */
                                    // define the number of row visible by page
                                    /*pageSize: 50,*/

                                    /* This property enables the serverSide part */
                                    serverSide: true,

                                    // Once the table is displayed, we update the paginator state
                                    tableRendered: function () {
                                        updatePaginator(this);
                                    },
                                    tableLoaded: function () {
                                        datagrid.initializeGrid(this);
                                    },
                                    modelChanged: function (rowIndex, columnIndex, oldValue, newValue, row) {
                                        updateCellValue(this, rowIndex, columnIndex, oldValue, newValue, row);
                                    }
                                });
                                this.fetchGrid();

                                $("#filter").val(this.editableGrid.currentFilter != null ? this.editableGrid.currentFilter : "");
                                if (this.editableGrid.currentFilter != null && this.editableGrid.currentFilter.length > 0)
                                    $("#filter").addClass('filterdefined');
                                else
                                    $("#filter").removeClass('filterdefined');

                            }

                            DatabaseGrid.prototype.fetchGrid = function () {
                                // call a PHP script to get the data
                                this.editableGrid.loadJSON("<?php echo base_url(); ?>index.php/administrator/user_management/crud_user_load");
                            };

                            DatabaseGrid.prototype.initializeGrid = function (grid) {

                                var self = this;

                                // render for the action column
                                grid.setCellRenderer("action", new CellRenderer({
                                    render: function (cell, ID_USER) {
                                        cell.innerHTML += "<i onclick=\"getUserById(" + ID_USER + ")\" class='fa fa-edit' data-toggle='modal' data-target='#modalAddUser'></i>&nbsp;";
                                        cell.innerHTML += "<i onclick=\"confirmDeleteUser(" + ID_USER + ")\" class='fa fa-trash'></i>&nbsp;";
                                    }
                                }));

                                grid.renderGrid("tablecontent", "testgrid");

                            };

                            DatabaseGrid.prototype.deleteRow = function (id)
                            {

                                var self = this;

                                if (confirm('Are you sur you want to delete the row id ' + id)) {

                                    $.ajax({
                                        url: '<?php echo base_url(); ?>index.php/administrator/projects/view_vcustomer_delete',
                                        type: 'POST',
                                        dataType: "html",
                                        data: {
                                            tablename: self.editableGrid.name,
                                            id: id
                                        },
                                        success: function (response)
                                        {
                                            if (response == "ok") {
                                                message("success", "Row deleted");
                                                self.fetchGrid();
                                            }
                                        },
                                        error: function (XMLHttpRequest, textStatus, exception) {
                                            alert("Ajax failure\n" + errortext);
                                        },
                                        async: true
                                    });


                                }

                            };


                            DatabaseGrid.prototype.addRow = function (id)
                            {
                                var self = this;
                                $.ajax({
                                    url: '<?php echo base_url(); ?>index.php/administrator/projects/view_vcustomer_add',
                                    type: 'POST',
                                    dataType: "html",
                                    data: {
                                        tablename: self.editableGrid.name,
                                        name: $("#name").val(),
                                        firstname: $("#firstname").val()
                                    },
                                    success: function (response)
                                    {
                                        if (response == "ok") {

                                            // hide form
                                            showAddForm();
                                            $("#name").val('');
                                            $("#firstname").val('');
                                            message("success", "Row added : reload model");
                                            self.fetchGrid();
                                        } else
                                            message("error", "Error occured");
                                    },
                                    error: function (XMLHttpRequest, textStatus, exception) {
                                        alert("Ajax failure\n" + errortext);
                                    },
                                    async: true
                                });
                            };

                            function updatePaginator(grid, divId)
                            {
                                divId = divId || "paginator";
                                var paginator = $("#" + divId).empty();
                                var nbPages = grid.getPageCount();

                                // get interval
                                var interval = grid.getSlidingPageInterval(20);
                                if (interval == null)
                                    return;

                                // get pages in interval (with links except for the current page)
                                var pages = grid.getPagesInInterval(interval, function (pageIndex, isCurrent) {
                                    if (isCurrent)
                                        return "<span id='currentpageindex'>" + (pageIndex + 1) + "</span>";
                                    return $("<a>").css("cursor", "pointer").html(pageIndex + 1).click(function (event) {
                                        grid.setPageIndex(parseInt($(this).html()) - 1);
                                    });
                                });

                                // "first" link
                                var link = $("<a class='nobg'>").html("<i class='fa fa-fast-backward'></i>");
                                if (!grid.canGoBack())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.firstPage();
                                    });
                                paginator.append(link);

                                // "prev" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-backward'></i>");
                                if (!grid.canGoBack())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.prevPage();
                                    });
                                paginator.append(link);

                                // pages
                                for (p = 0; p < pages.length; p++)
                                    paginator.append(pages[p]).append(" ");

                                // "next" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-forward'>");
                                if (!grid.canGoForward())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.nextPage();
                                    });
                                paginator.append(link);

                                // "last" link
                                link = $("<a class='nobg'>").html("<i class='fa fa-fast-forward'>");
                                if (!grid.canGoForward())
                                    link.css({opacity: 0.4, filter: "alpha(opacity=40)"});
                                else
                                    link.css("cursor", "pointer").click(function (event) {
                                        grid.lastPage();
                                    });
                                paginator.append(link);
                            }
                            ;
                        </script>
                        <script type="text/javascript">
                            var datagrid;
                            window.onload = function () {
                                datagrid = new DatabaseGrid();
                                // key typed in the filter field
                                $("#filter").keyup(function () {
                                    datagrid.editableGrid.filter($(this).val());

                                    // To filter on some columns, you can set an array of column index
                                    //datagrid.editableGrid.filter( $(this).val(), [0,3,5]);
                                });

                                $("#addbutton").click(function () {
                                    datagrid.addRow();
                                });
                            };

                            $(function () {
                                $("#dialog").dialog({
                                    autoOpen: false,
                                    modal: true,
                                    height: 600,
                                    width: 800,
                                    open: function (ev, ui) {
                                        $('#add_iframe').attr('src', $('#dialog').data('src'));
                                    },
                                    close: function (ev, ui) {
                                        location.reload();
                                    }
                                });
                            });

                        </script>
                        <!-- simple form, used to add a new row -->
                        <div id="dialog" style="overflow: hidden;">
                            <iframe id="add_iframe" src="" style="border: none; width: 100%; height: 100%; overflow: hidden;"></iframe>
                        </div>
                    </section>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
</section>

<script type="text/javascript">
    $('#btnCreateNew').on('click', function () {
        $('#btn-save').html('Save')
        $('#form-user-title').html("Add New User");
        $('#name').val('');
        $('#username').val('');
        $('#pwd').val('');
        $('#access').val(1);
        $('#customer').val(1);
        $('#work_area').val(1);
    })

    function getUserById(id) {
        $('#btn-save').html('Update');
        $('#form-user-title').html("Edit User");
        var url_location = '<?php echo base_url() ?>' + 'index.php/administrator/user_management/get_user_by_id?id=' + id;
        $.ajax({
            type: "GET",
            url: url_location,
            dataType: "text",
            cache: false,
            success: function (data) {
                var msg = $.parseJSON(data);
                if (msg.status === 'success') {
                    var user = msg.data;
                    $('#user_id').val(user.ID_USER);
                    $('#name').val(user.NAME);
                    $('#username').val(user.USERNAME);
                    $('#pwd').val(user.PASSWORD);
                    $('#access').val(user.GROUP_ID);
                    $('#customer').val(user.CUSTOMER_ID);
                    $('#customer').select2({
                        placeholder: user.CUSTOMER_ID
                    });
//                        $('#work_area').val(user.WORK_AREA_ID);
                }
            }
        });

    }

    function confirmDeleteUser(id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this user!",
            icon: "warning",
            buttons: true,
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {
                        var url_location = '<?php echo base_url() ?>' + 'index.php/administrator/user_management/delete_user_by_id?id=' + id;
                        $.ajax({
                            type: "GET",
                            url: url_location,
                            dataType: "text",
                            cache: false,
                            success: function (data) {
                                var msg = $.parseJSON(data);
                                if (msg.status === 'failed') {

                                } else {
                                    swal("User has been deleted!", {
                                        icon: "success"
                                    });
                                    datagrid.fetchGrid();
                                }
                            }
                        });
                    }
                });
    }

</script>