<div id="modalAddUserGroup" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="form-user-title"></h4>
        </div>
        <div class="modal-body">
            <div class="form-horizontal">
                <div id="msg" class="alert">
                </div>
                <input type="text" class="form-control" id="user_gorup_id"  style="display:none">             
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">User Group</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" placeholder="Enter User Group">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="level">Level</label>
                    <div class="col-sm-10">
                        <select name="PARENT" id="level" class="form-control">
                            <?php for ($i=1; $i <= 4 ; $i++) { ?>
                                <option value="<?php echo $i; ?>" > <?php echo $i; ?> </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>                                                                                                               
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btn-save">Save</button>
        </div>
    </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#msg').hide();
        $('#btn-save').on("click", function(){
            var btn = $('#btn-save').html();
            var url_location = '';
            if(btn === 'Save'){
                url_location = '<?php echo base_url() ?>' + 'admin/user_management/add_new_users_group';
            }else{
                url_location = '<?php echo base_url() ?>' + 'admin/user_management/update_user_group';
            }
            
            var data = {   
                        id: $('#user_gorup_id').val(),
                        name: $('#name').val(),
                        level: $('#level').val()
                    }

            $.ajax({
                type: "POST",
                url: url_location,
                data: data,
                dataType: "text",
                cache: false,
                success: function(data){
                    var msg = $.parseJSON(data);
                    if(msg.status === 'failed'){
                        swal(msg.body_msg, {
                                icon: "error"
                        });
                    }else{
                        swal(msg.body_msg, {
                                icon: "success"
                        });
                        datagrid.fetchGrid();
                    }
                }
            }); 
        })
    })
</script>