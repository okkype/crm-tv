<section class="content-header">
    <h1 align="center">
        <u>Overview</u>
        <br>
        802696
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->
<div class="container">
      <div class="row">
        <div class="col-sm-6" style="background-color:white;">
          <!-- #f4f1f4 -->
          <th>Engine Owner : Danu</th><br>
          <th>Customer : Garuda Indonesia</th><br>
          <th>Workscope : Minimum</th>
        </div>
        <div class="col-sm-6" style="background-color:white;">
          <th>Type : CFM56-7B26</th><br>
          <th>Induction Date  : 16 Januari 2017</th><br>
          <th>Contractual TAT  : 14</th>
        </div>
      </div>
</div>
<br>
<div class="container">
  <div class="row">
    <div class="col-sm-4" style="background-color:white;">
          <br>
          <div class="info-box">
            <span class="info-box-icon" style="background-color: #65b328;"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Actual TAT</span>
              <span class="info-box-number">26 Days</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
           <br>
          <div class="info-box">
            <span class="info-box-icon" style="background-color: #0dacbe;"><i class="fa fa-list-alt"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Job Card</span>
              <span class="info-box-number">28 Days</span>
              <span class="info-box-text">364 of 975</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
    </div>

    <div class="col-sm-8" style="background-color:white;"><br>
      <div class="box-body">
              <div class="col-sm-12 container">
                <div class="progress">
                  <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:05%">
                    5% Complete (Open)
                  </div>
                </div>
                <div class="progress">
                  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:67%">
                    67% Complete (In Progress)
                  </div>
                </div>
                <div class="progress">
                  <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:28%">
                    28% Complete (Closed)
                  </div>
                </div>
                <br>
                  <div class="row">
                  <!-- <div class="container"> -->
                      <div class="col-sm-12">          
                    <table class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>NO</th>
                          <th>MAT</th>
                          <th>DESCRIPTION</th>
                          <th>STATUS</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>REM</td>
                          <td>FUEL TUBES P7&PB</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>REM</td>
                          <td>FWD SUMP OILSUPLY TUBE & ANTISIPHON TUBE</td>
                          <td>In Progress</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>REM</td>
                          <td>P12&PS12 AIRMANIFOLD, PS12 AIR SUPPLYTUBE</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>4</td>
                          <td>REM</td>
                          <td>N1 SPEED SENSOR GUIDE TUBE</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>5</td>
                          <td>REM</td>
                          <td>TUBING GUIDE TUBE</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>6</td>
                          <td>REM</td>
                          <td>OIL SCAVENGE TUBES</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>7</td>
                          <td>REM</td>
                          <td>OIL SUPPLY TUBES</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>8</td>
                          <td>REM</td>
                          <td>NO1 BEARING VIBR SENSOR FRONT GUIDE TUBE</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>9</td>
                          <td>REM</td>
                          <td>NO1 BEARING VIBR SENSOR REAR GUIDE TUBE</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>10</td>
                          <td>REM</td>
                          <td>NO1 BEARING VIBR SENSOR REAR GUIDE TUBE</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>11</td>
                          <td>REM</td>
                          <td>AGB RH MOUNT LINK ASSEMBLY</td>
                          <td>In Progress</td>
                        </tr>
                        <tr>
                          <td>12</td>
                          <td>REM</td>
                          <td>AGB LH MOUNT LINK ASSEMBLY</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>13</td>
                          <td>DSY</td>
                          <td>OIL TANK UPPER SHOCK MOUNT</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>14</td>
                          <td>DSY</td>
                          <td>TGB REAR CLEVIS MOUNT</td>
                          <td>Closed</td>
                        </tr>
                        <tr>
                          <td>15</td>
                          <td>DSY</td>
                          <td>AGB BRACKET</td>
                          <td>In Progress</td>
                        </tr>
                        <tr>
                          <td>16</td>
                          <td>DSY</td>
                          <td>VARIABLE BLEED VALVE FEEDBACK ROD</td>
                          <td>In Progress</td>
                        </tr>
                        <tr>
                          <td>17</td>
                          <td>DSY</td>
                          <td>BRACKET</td>
                          <td>Open</td>
                        </tr>
                      </tbody>
                    </table>
                  <!-- </div> -->
                    
                  </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col (right) -->
      </div>
  </div>
</div>

</div>

   
</section>