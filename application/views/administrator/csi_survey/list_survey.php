<section class="content-header">
    <h1>
        List CSI Survey
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">list csi survey</li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List Survey</h3>
                </div>
                <div class="box-body">
                    <form method="post" action="<?php echo base_url() ?>index.php/<?php if ($this->data["session"]["group_id"] == "1"){echo "administrator";}elseif ($this->data["session"]["group_id"] == "2"){echo "planner";}?>/production_planning/top_project">
                        <table class="tabledata table table-bordered table-striped" width="100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>NAME</th>
                                    <th>COMPANY</th>
                                    <th>ESN</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 0;
                                foreach ($survey as $row):
                                    $no ++;
                                    ?>
                                <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $row->NAME; ?></td>
                                        <td><?php echo $row->COMPANY; ?></td>
                                        <td><?php echo $row->ESN; ?></td>
                                        <td>
                                            <a class='btn btn-primary my-modal' data-target='.mymodal' data-cache='false' data-toggle='modal' data-href= "<?php echo base_url(); ?>index.php/administrator/csi_survey/view_survey/<?php echo $row->ID_SURVEY; ?>" href='javascript:void(0);'><i class='fa fa-pencil' aria-hidden='true'></i> View Survey</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade mymodal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
    </div>
</div>
